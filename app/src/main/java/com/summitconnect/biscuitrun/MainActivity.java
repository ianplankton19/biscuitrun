package com.summitconnect.biscuitrun;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.webkit.WebView;
import android.widget.Toast;

import com.summitconnect.biscuitrun.base.BaseActivity;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.data.api.request.LogRequest;
import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GenericResponse;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.AudioControlEvent;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.events.ExitApp;
import com.summitconnect.biscuitrun.events.OnGameEndEvent;
import com.summitconnect.biscuitrun.events.OnGameStartEvent;
import com.summitconnect.biscuitrun.events.StartOnGameActivityEvent;
import com.summitconnect.biscuitrun.fragments.CongratulationsFragment;
import com.summitconnect.biscuitrun.fragments.GameOverFragment;
import com.summitconnect.biscuitrun.fragments.LevelClearedFragment;
import com.summitconnect.biscuitrun.fragments.LoadingFragment;
import com.summitconnect.biscuitrun.fragments.SplashFragment;
import com.summitconnect.biscuitrun.helpers.LogHelper;
import com.summitconnect.biscuitrun.helpers.WebviewHelper;
import com.summitconnect.biscuitrun.helpers.encryption.EncryptionUtil;
import com.summitconnect.biscuitrun.managers.AudioManager;
import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.HttpException;

public class MainActivity extends BaseActivity implements OnApiRequestListener {

    String userId;
    String email;
    String code;
    int selectedCharacter;

    private boolean doubleBackToExitPressedOnce = false;
    private AudioManager audioManager;
    public boolean isAudioOn = true;
    private ApiRequestHelper requestHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setFragment(SplashFragment.newInstance());
        initAudio();
        audioManager.playAudio(MainMusicType.SPLASH);
        //set api helper
        requestHelper = new ApiRequestHelper(this);
        //load webview data to cache
        loadWebViewCache();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAudioOn) {
            audioManager.resumeAudio();
        }
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        if (isAudioOn) {
            audioManager.pauseAudio();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void exitApp(ExitApp event) {
        finish();
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void startOnGameActivity(StartOnGameActivityEvent event) {
        selectedCharacter = event.getSelectedCharacter();
        setFragment(LoadingFragment.newInstance(getStringFromResource(R.string.all_loading)));
        EventBus.getDefault().postSticky(new OnGameStartEvent(event.getLevel(), selectedCharacter,
                event.getScore(), event.getGeneratedStar(), hasCode(), event.isHasLife()));
        EventBus.getDefault().removeStickyEvent(event);
        //Delay opening activity
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goToOtherActivity(OnGameActivity.class);
            }
        }, 1000);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onGameEnd(OnGameEndEvent event) {
        if (event.isGameOver()) {
            //Goto GameOver Fragment
            setFragment(GameOverFragment.newInstance(event.isHasLife(), event.getLevelFinished(),
                    event.getSelectedCharacter(), event.getScore(), event.getStarGeneratedCount()));
            //Save score upon game over
            saveScore(event.getScore());
        } else {
            if (event.getLevelFinished() == 6) {
                //Goto Congratulations Fragment
                setFragment(CongratulationsFragment.newInstance(event.isHasLife(), event.getLevelFinished(),
                        event.getSelectedCharacter(), event.getScore(), event.getStarGeneratedCount()));
                //Save score upon game complete
                saveScore(event.getScore());
            } else {
                //Goto LevelCleared Fragment
                setFragment(LevelClearedFragment.newInstance(event.isHasLife(), event.getLevelFinished() + 1,
                        event.getScore(), event.getStarGeneratedCount(), event.getSelectedCharacter()));
            }
        }
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void changeFragment(ChangeFragmentEvent event) {
        setFragment(event.getFragment());
    }

    public void audioControl(AudioControlMode mode, MainMusicType type) {
        switch (mode) {
            case PLAY:
                audioManager.playAudio(type);
                isAudioOn = true;
                EventBus.getDefault().postSticky(new AudioControlEvent(isAudioOn));
                break;

            case PAUSE:
                audioManager.pauseAudio();
                isAudioOn = false;
                EventBus.getDefault().postSticky(new AudioControlEvent(isAudioOn));
                break;

            case STOP:
                audioManager.stopAudio();
                isAudioOn = false;
                EventBus.getDefault().postSticky(new AudioControlEvent(isAudioOn));
                break;

            default:
                break;

        }
    }

    /*
    *  This is used when you want to restrict audio without affecting
    *  isAudio
    * */
    public void doNotPlaySound() {
        audioManager.stopAudio();
    }

    /*
    * Check if media player is currently playing
    * */
    public boolean isAudioPlaying() {
        return audioManager.getPlayer().isPlaying();
    }


    @Override
    public void onApiRequestStart(ApiAction apiAction) {
//        setProgressDialogMessage("Please wait...");
//        showLoading();
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {
        dismissLoading();
        LogHelper.log("err", "action --> " + apiAction.name() + " cause --> " + t.getMessage());
        if (t.getMessage().contains("failed to connect") || t.getMessage().contains("Failed to connect")) {
            showFailedToConnectError();
        } else if (t.getMessage().contains("timeout")) {
            showSocketTimeoutError();
        } else if (t.getMessage().contains("No address associated with hostname") ||
                t.getMessage().contains("Unable to resolve host")) {
            showNoConnectionError();
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = ((HttpException) t);
                try {
                    final String json = ex.response().errorBody().string();
                    final GenericResponse genericResponse = requestHelper.parseError(GenericResponse.class, json);
                    showToast(genericResponse.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    showToast(getString(R.string.all_message_error_unknown));
                }
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {
        dismissLoading();
        if (apiAction.equals(ApiAction.LOG)) {
            if (((BaseResponse) result).isSuccess() == 1) {
                LogHelper.log("MainActivity", "Score saved.");
            } else
                LogHelper.log("MainActivity", ((BaseResponse) result).getMessage());
        }
    }

    public void initAudio() {
        audioManager = new AudioManager(this);
    }

    public void setFragment(final Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, fragment);
        transaction.commit();
    }

    private void saveScore(int score) {
        LogHelper.log("MainActivity", "email: " + email);
        if (isConnected()) {
            requestHelper.log(new LogRequest(userId, email, code, String.valueOf(score)));
        } else {
            showNoConnectionError();
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private boolean hasCode() {
        return code != null && !code.isEmpty() && !code.equals("none") && !code.equals("free");
    }

    private void loadWebViewCache() {
        String url = EncryptionUtil.decrypt(BuildConfig.AES,BuildConfig.BASE_URL);
        WebView webView1 = new WebView(getApplicationContext());
        WebviewHelper.loadUrl(this, webView1, url + "objects/rules.php", true);

        WebView webView2 = new WebView(getApplicationContext());
        WebviewHelper.loadUrl(this, webView2, url + "objects/howto.php", true);

        WebView webView3 = new WebView(getApplicationContext());
        WebviewHelper.loadUrl(this, webView3, url + "objects/about.php", true);

        WebView webView4 = new WebView(getApplicationContext());
        WebviewHelper.loadUrl(this, webView4, url + "objects/sponsors.php", true);

        WebView webView5 = new WebView(getApplicationContext());
        WebviewHelper.loadUrl(this, webView5, url + "objects/howgamecode.php", true);
    }

}
