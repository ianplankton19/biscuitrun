package com.summitconnect.biscuitrun.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.base.BaseActivity;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jess on 20/05/2017.
 */

public class HighScoreAdapter extends RecyclerView.Adapter<HighScoreAdapter.ViewHolder> {

    BaseActivity activity;
    Context context;
    ArrayList<Map.Entry<String, JsonElement>> data;

    public HighScoreAdapter(Context context, ArrayList<Map.Entry<String, JsonElement>> data) {
        this.context = context;
        this.data = data;
        this.activity = (BaseActivity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_highscore, parent, false);
        ViewHolder vh = new ViewHolder(view);


        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        JsonObject highScoreData = data.get(position).getValue().getAsJsonObject();
        String title = highScoreData.get("title").getAsString();
        JsonArray posts = highScoreData.get("posts").getAsJsonArray();
        holder.tvTitle.setText(title);

        for (int i = 0; i < posts.size(); i++) {
            View child = LayoutInflater.from(holder.linScoreContainer.getContext()).inflate(R.layout.column_layout, null);
            TextView tvName = ButterKnife.findById(child, R.id.tvName);
            TextView tvRank = ButterKnife.findById(child, R.id.tvRank);
            TextView tvScore = ButterKnife.findById(child, R.id.tvScore);

            if (i == 0){
                activity.setBackgroundImage(tvName, R.drawable.dotted_dash_no_right);
                activity.setBackgroundImage(tvRank, R.drawable.dotted_dash_full);
                activity.setBackgroundImage(tvScore, R.drawable.dotted_dash_no_left);

            }

            JsonObject post = posts.get(i).getAsJsonObject();
            String name = post.get("name").getAsString();
            String score = post.get("score").getAsString();
            String rank = String.valueOf(i + 1);

            tvName.setText(name);
            tvRank.setText(rank);
            tvScore.setText(score);


            holder.linScoreContainer.addView(child);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.linScoreContainer)
        LinearLayout linScoreContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
