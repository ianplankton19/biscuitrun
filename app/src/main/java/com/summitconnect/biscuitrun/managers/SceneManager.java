package com.summitconnect.biscuitrun.managers;

import com.summitconnect.biscuitrun.base.BaseScene;
import com.summitconnect.biscuitrun.scenes.ChooseCharacterScene;

import org.andengine.engine.Engine;

import static com.summitconnect.biscuitrun.managers.SceneManager.SceneType.SCENE_CHOOSECHARACTER;

/**
 * Created by jayan on 5/14/2017.
 */

public class SceneManager
{
    //---------------------------------------------
    // SCENES
    //---------------------------------------------

    private BaseScene chooseCharacterScene;

    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------

    private static final SceneManager INSTANCE = new SceneManager();

    private SceneType currentSceneType = SCENE_CHOOSECHARACTER;

    private BaseScene currentScene;

    private Engine engine = ResourcesManager.getInstance().engine;

    public enum SceneType
    {
        SCENE_CHOOSECHARACTER
    }

    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------

    public void setScene(BaseScene scene) {
            engine.setScene(scene);
            currentScene = scene;
            currentSceneType = scene.getSceneType();
    }

    public void setScene(SceneType sceneType)
    {
        //Set the new scene
        switch (sceneType)
        {
            case SCENE_CHOOSECHARACTER:
                setScene(chooseCharacterScene);
                break;
            default:
                break;
        }
    }

    public void createChooseCharacterScene()
    {
        ResourcesManager.getInstance().loadChooseCharacterScreen();
        chooseCharacterScene = new ChooseCharacterScene();
        currentScene = chooseCharacterScene;
//        pOnCreateSceneCallback.onCreateSceneFinished(chooseCharacterScene);
    }

    private void disposeChooseCharacterScene()
    {
        ResourcesManager.getInstance().unloadChooseCharacterScreen();
        chooseCharacterScene.disposeScene();
        chooseCharacterScene = null;
    }

    private void disposeCurrentScene(){
        //Set the new scene
        switch (currentSceneType)
        {
            case SCENE_CHOOSECHARACTER:
                disposeChooseCharacterScene();
                break;
            default:
                break;
        }
    }

    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------

    public static SceneManager getInstance()
    {
        return INSTANCE;
    }

    public SceneType getCurrentSceneType()
    {
        return currentSceneType;
    }

    public BaseScene getCurrentScene()
    {
        return currentScene;
    }
}