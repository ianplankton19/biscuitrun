package com.summitconnect.biscuitrun.managers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import com.summitconnect.biscuitrun.enums.MainMusicType;

import java.io.IOException;

/**
 * Created by jess on 18/05/2017.
 */

public class AudioManager {

    private Context context;
    private MediaPlayer player;

    public AudioManager(Context context) {
        this.context = context;
    }

    private void loadAudio(String filename) {
        AssetFileDescriptor afd = null;
        try {
            afd = context.getAssets().openFd(filename);
            player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.setLooping(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playAudio(MainMusicType type) {
        if (player != null)
            stopAudio();

        switch (type) {
            case SPLASH:
                loadAudio("mfx/sfx_splash.m4a");
                break;

            case LOBBY:
                loadAudio("mfx/music_lobby.m4a");
                break;

            case LVL_CLEAR:
                loadAudio("mfx/sfx_levelcleared.m4a");
                player.setLooping(false);
                break;

            case GAMEOVER:
                loadAudio("mfx/sfx_gameover.m4a");
                player.setLooping(false);
                break;

            case CONGRATULATIONS:
                loadAudio("mfx/sfx_gameover_all.m4a");
                player.setLooping(false);
                break;

            default:
                break;
        }


        player.start();

    }

    public void resumeAudio() {
        if(player != null)
            player.start();
    }

    public void pauseAudio() {
        if (player != null) {
            if (player.isPlaying())
                player.pause();
        }
    }

    public void stopAudio() {
        if(player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }

    public MediaPlayer getPlayer() {
        return player;
    }
}
