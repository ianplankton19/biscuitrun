package com.summitconnect.biscuitrun.managers;

import android.graphics.Color;

import com.summitconnect.biscuitrun.GameActivity;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/**
 * Created by jayan on 5/14/2017.
 */

public class ResourcesManager {
    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------

    private static final ResourcesManager INSTANCE = new ResourcesManager();

    public Engine engine;
    public GameActivity activity;
    public Camera camera;
    public VertexBufferObjectManager vbom;

    public Font font;

    //---------------------------------------------
    // TEXTURES & TEXTURE REGIONS
    //---------------------------------------------

    /**Login**/
    public ITextureRegion loginBgRegion;
    private BitmapTextureAtlas loginBgTextureAtlas;
    public ITextureRegion loginLabelEmailRegion;
    private BitmapTextureAtlas loginLabelEmailTextureAtlas;
    public ITextureRegion loginLabelPasswordRegion;
    private BitmapTextureAtlas loginLabelPasswordTextureAtlas;

    public ITextureRegion btnLoginRegion;
    private BitmapTextureAtlas btnLoginTextureAtlas;
    public ITextureRegion btnSignUpRegion;
    private BitmapTextureAtlas btnSignUpTextureAtlas;
    public ITextureRegion btnForgotPasswordRegion;
    private BitmapTextureAtlas btnForgotPasswordTextureAtlas;

    /**Global**/
    public ITextureRegion overallBgRegion;
    private BitmapTextureAtlas overallBgTextureAtlas;
    public ITextureRegion panelBgRegion;
    private BitmapTextureAtlas panelBgTextureAtlas;
    public ITextureRegion btnCloseRegion;
    private BitmapTextureAtlas btnCloseTextureAtlas;
    public ITiledTextureRegion btnAudioToggleRegion;
    private BitmapTextureAtlas btnAudioToggleTextureAtlas;
    public ITextureRegion btnSubmitRegion;
    private BitmapTextureAtlas btnSubmitTextureAtlas;
    public ITextureRegion btnNextRegion;
    private BitmapTextureAtlas btnNextTextureAtlas;
    public ITextureRegion btnOkRegion;
    private BitmapTextureAtlas btnOkTextureAtlas;
    public ITextureRegion btnSkipRegion;
    private BitmapTextureAtlas btnSkipTextureAtlas;

    /**SignUp**/
    public ITextureRegion headerRegistrationRegion;
    private BitmapTextureAtlas headerRegistrationTextureAtlas;

    /**ForgotPassword**/
    public ITextureRegion headerForgotPasswordRegion;
    private BitmapTextureAtlas headerForgotPasswordTextureAtlas;

    /**Menu**/
    public ITextureRegion menuBgRegion;
    private BitmapTextureAtlas menuBgTextureAtlas;
    public ITextureRegion btnLogoutRegion;
    private BitmapTextureAtlas btnLogoutTextureAtlas;
    public ITextureRegion btnStartRegion;
    private BitmapTextureAtlas btnStartTextureAtlas;
    public ITextureRegion btnGameRulesRegion;
    private BitmapTextureAtlas btnGameRulesTextureAtlas;
    public ITextureRegion btnHowToPlayRegion;
    private BitmapTextureAtlas btnHowToPlayTextureAtlas;
    public ITextureRegion btnAboutRegion;
    private BitmapTextureAtlas btnAboutTextureAtlas;
    public ITextureRegion btnHighscoreRegion;
    private BitmapTextureAtlas btnHighscoreTextureAtlas;
    public ITextureRegion btnSponsorsRegion;
    private BitmapTextureAtlas btnSponsorsTextureAtlas;

    /**Game Rule**/
    public ITextureRegion headerGameRuleRegion;
    private BitmapTextureAtlas headerGameRuleTextureAtlas;

    /**How To Play**/
    public ITextureRegion headerHowToPlayRegion;
    private BitmapTextureAtlas headerHowToPlayTextureAtlas;

    /**About**/
    public ITextureRegion headerAboutRegion;
    private BitmapTextureAtlas headerAboutTextureAtlas;

    /**Sponsors**/
    public ITextureRegion headerSponsorsRegion;
    private BitmapTextureAtlas headerSponsorsTextureAtlas;

    /**Game Code**/
    public ITextureRegion headerGameCodeRegion;
    private BitmapTextureAtlas headerGameCodeTextureAtlas;
    public ITextureRegion btnHowToGameCodeRegion;
    private BitmapTextureAtlas btnHowToGameCodeTextureAtlas;

    /**Highscore**/
    public ITextureRegion highscoreBgRegion;
    private BitmapTextureAtlas highscoreBgTextureAtlas;

    /**ChooseCharacter**/
    public TiledTextureRegion mFaceTextureRegionChocoKnot;
    private BitmapTextureAtlas mBitmapTextureAtlasChocoKnot;
    public TiledTextureRegion mFaceTextureRegionCookie;
    private BitmapTextureAtlas mBitmapTextureAtlasCookie;
    public TiledTextureRegion mFaceTextureRegionBiscuit;
    private BitmapTextureAtlas mBitmapTextureAtlasBiscuit;
    public ITextureRegion mTextureRegionSelect;
    private BitmapTextureAtlas mTextureSelect;
    public ITextureRegion mITextureRegionTitle;
    private BitmapTextureAtlas mTextureTitle;
    public ITextureRegion mITextureRegionGlass;
    public BitmapTextureAtlas mTextureGlass;
    public TextureRegion backgroundTextureRegion;
    private BitmapTextureAtlas backgroundTextureAtlas;
    public Music musicChoose;
    public Sound soundCharChoose, soundSelect;


    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------

    private void loadFonts()
    {
        FontFactory.setAssetBasePath("fonts/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "arial-rounded-mt-bold.ttf", 25, true, Color.WHITE, 2, Color.WHITE);
        font.load();
    }

    public void loadChooseCharacterResources()
    {
        loadChooseCharacterGraphics();
        loadChooseCharacterAudio();
        loadFonts();
    }

    private void loadChooseCharacterGraphics()
    {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        mBitmapTextureAtlasChocoKnot = new BitmapTextureAtlas(activity.getTextureManager(), 448, 316, TextureOptions.BILINEAR);
        mFaceTextureRegionChocoKnot = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mBitmapTextureAtlasChocoKnot, activity, "character_pretzel.png", 0, 0, 2, 2);
        mBitmapTextureAtlasChocoKnot.load();
        mBitmapTextureAtlasCookie = new BitmapTextureAtlas(activity.getTextureManager(), 423, 329, TextureOptions.BILINEAR);
        mFaceTextureRegionCookie = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mBitmapTextureAtlasCookie, activity, "character_cookie.png", 0, 0, 2, 2);
        mBitmapTextureAtlasCookie.load();
        mBitmapTextureAtlasBiscuit = new BitmapTextureAtlas(activity.getTextureManager(), 304, 320, TextureOptions.BILINEAR);
        mFaceTextureRegionBiscuit = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mBitmapTextureAtlasBiscuit, activity, "character_biscuit.png", 0, 0, 2, 2);
        mBitmapTextureAtlasBiscuit.load();
        mTextureSelect = new BitmapTextureAtlas(activity.getTextureManager(), 195, 108, TextureOptions.BILINEAR);
        mTextureRegionSelect = BitmapTextureAtlasTextureRegionFactory
                .createFromAsset(mTextureSelect, activity, "btn_select.png", 0, 0);
        mTextureSelect.load();
        mTextureTitle = new BitmapTextureAtlas(activity.getTextureManager(), 924, 62, TextureOptions.BILINEAR);
        mITextureRegionTitle = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureTitle,
                activity, "choose_character_title.png", 0, 0);
        mTextureTitle.load();
        mTextureGlass = new BitmapTextureAtlas(activity.getTextureManager(), 270, 340, TextureOptions.BILINEAR);
        mITextureRegionGlass = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureGlass,
                activity, "glass_display.png", 0, 0);
        mTextureGlass.load();
        backgroundTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(),
                1136, 640);
        backgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTextureAtlas,
                activity, "bg_choose_character.png", 0, 0);
        backgroundTextureAtlas.load();


    }

    private void loadChooseCharacterAudio()
    {
        MusicFactory.setAssetBasePath("mfx/");
        try {
            this.musicChoose = MusicFactory.createMusicFromAsset(activity.getEngine().getMusicManager(), activity.getApplicationContext(), "sfx_choose.m4a");

        } catch (final IOException e) {
            Debug.e(e);
        }

        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.soundCharChoose = SoundFactory.createSoundFromAsset(activity.getEngine().getSoundManager(), activity.getApplicationContext(), "sfx_choose_glass.m4a");
            this.soundSelect = SoundFactory.createSoundFromAsset(activity.getEngine().getSoundManager(), activity.getApplicationContext(), "sfx_choose_btn.m4a");
        } catch (final IOException e) {
            Debug.e(e);
        }

    }


    public void loadChooseCharacterScreen()
    {
        loadChooseCharacterResources();
    }

    public void unloadChooseCharacterScreen()
    {
        mFaceTextureRegionChocoKnot = null;
        mBitmapTextureAtlasChocoKnot.unload();

        mFaceTextureRegionCookie = null;
        mBitmapTextureAtlasCookie.unload();

        mFaceTextureRegionBiscuit = null;
        mBitmapTextureAtlasBiscuit.unload();

        mTextureRegionSelect = null;
        mTextureSelect.unload();

        mITextureRegionTitle = null;
        mTextureTitle.unload();

        mITextureRegionGlass = null;
        mTextureGlass.unload();

        backgroundTextureRegion = null;
        backgroundTextureAtlas.unload();
        
    }

    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom
     * <br><br>
     * We use activity method at beginning of game loading, to prepare Resources Manager properly,
     * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom)
    {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }

    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------

    public static ResourcesManager getInstance()
    {
        return INSTANCE;
    }
}
