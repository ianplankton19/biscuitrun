package com.summitconnect.biscuitrun;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.summitconnect.biscuitrun.events.AudioControlEvent;
import com.summitconnect.biscuitrun.events.ExitApp;
import com.summitconnect.biscuitrun.managers.ResourcesManager;
import com.summitconnect.biscuitrun.scenes.ChooseCharacterScene;

import org.andengine.audio.sound.Sound;
import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by jayan on 5/14/2017.
 */

public class GameActivity extends SimpleBaseGameActivity {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private boolean doubleBackToExitPressedOnce = false;
    private final int CAMERA_WIDTH = 1136;
    protected final int CAMERA_HEIGHT = 640;
    private Camera camera;
    private ResourcesManager resourcesManager;
    private Toast toast = null;

    private LinearLayout linLoginContainer;
    private EditText txtEmail, txtPassword;

    public boolean isAudioOn = true;


    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        return new LimitedFPSEngine(pEngineOptions, 60);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), this.camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;
    }

    @Override
    protected void onCreateResources() {
        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
        resourcesManager = ResourcesManager.getInstance();
    }

    @Override
    protected Scene onCreateScene() {
        ResourcesManager.getInstance().loadChooseCharacterScreen();
        ChooseCharacterScene chooseCharacterScene = new ChooseCharacterScene();
        return chooseCharacterScene;
    }

//    @Override
//    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {
//        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
//        resourcesManager = ResourcesManager.getInstance();
//        pOnCreateResourcesCallback.onCreateResourcesFinished();
//    }
//
//    @Override
//    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
//        SceneManager.getInstance().createChooseCharacterScene(pOnCreateSceneCallback);
//    }
//
//    @Override
//    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
//        mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
//            public void onTimePassed(final TimerHandler pTimerHandler) {
//                mEngine.unregisterUpdateHandler(pTimerHandler);
//                // load menu resources, create menu scene
//                // set menu scene using scene manager
//                // disposeSplashScene();
//                // READ NEXT ARTICLE FOR THIS PART.
//            }
//        }));
//        pOnPopulateSceneCallback.onPopulateSceneFinished();
//    }

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        playMusic();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        stopMusic();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            EventBus.getDefault().postSticky(new ExitApp());
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    // this method dispatch the SoftKeyboard
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {
                //hide softKeyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }
        return ret;
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void audioControl(AudioControlEvent event) {
        isAudioOn = event.isAudioOn();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void playMusic() {
        if (this.resourcesManager != null && isAudioOn)
            this.resourcesManager.musicChoose.play();
    }

    public void stopMusic() {
        if (this.resourcesManager != null)
            this.resourcesManager.musicChoose.pause();
    }

    public void playSound(Sound sound) {
        if (this.resourcesManager != null && isAudioOn)
            sound.play();
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
