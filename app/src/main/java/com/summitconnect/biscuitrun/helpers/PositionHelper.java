package com.summitconnect.biscuitrun.helpers;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

/**
 * Created by jayan on 5/14/2017.
 */

public class PositionHelper {

    public static float centerXscreen(Camera camera, Sprite sprite){
        return  camera.getWidth() / 2 - sprite.getWidth()/2;
    }

    public static float centerYscreen(Camera camera, Sprite sprite){
        return  camera.getHeight() / 2 - sprite.getHeight()/2;
    }

    public static float centerXparent(Sprite parent, Sprite child){
        return  (parent.getX() + (parent.getWidth() / 2)) - (child.getWidth() / 2);
    }

    public static float centerXparent(Sprite parent, Text child){
        return  (parent.getX() + (parent.getWidth() / 2)) - (child.getWidth() / 2);
    }

    public static float centerYparent(Sprite parent, Sprite child){
        return  (parent.getY() + (parent.getHeight() / 2)) - (child.getHeight() / 2);
    }

    public static float centerYparent(Sprite parent, Text child){
        return  (parent.getY() + (parent.getHeight() / 2)) - (child.getHeight() / 2);
    }

    public static float bottomOfScreen(Camera camera, Sprite sprite){
        return camera.getHeight() - sprite.getHeight();
    }

    public static float getCenterHorizontalPoint(Sprite sprite){
        return sprite.getWidth() / 2;
    }

    public static float getCenterVerticalPoint(Sprite sprite){
        return sprite.getHeight() / 2;
    }



}
