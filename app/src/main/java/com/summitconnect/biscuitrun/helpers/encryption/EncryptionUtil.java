package com.summitconnect.biscuitrun.helpers.encryption;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;


/**
 * Created by jayanthony.lumba on 5/17/2016.
 */
public class EncryptionUtil {

    private static boolean isEncrypt = true;
    private static final String saltKey = "defaultSalt";

    public static String generateAesKey(String appSecretKey){
        AesCbcWithIntegrity.SecretKeys keys = null;
        try {

            keys = AesCbcWithIntegrity.generateKeyFromPassword(appSecretKey, saltKey);
            Log.d("KeyEncryptor","AES Key: " + keys.toString());
            return keys.toString();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encrypt(String aesKey, String str) {
        if (isEncrypt) {

            AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = null;
            try {
                AesCbcWithIntegrity.SecretKeys keys = AesCbcWithIntegrity.keys(aesKey);
                cipherTextIvMac = AesCbcWithIntegrity.encrypt(str, keys);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return cipherTextIvMac.toString();
        } else {
            return str;
        }
    }

    public static String decrypt(String aesKey, String cipherTextString) {
        String plainText = cipherTextString;
        if (isEncrypt) {
            //Use the constructor to re-create the CipherTextIvMac class from the string:
            try {
                AesCbcWithIntegrity.SecretKeys keys = AesCbcWithIntegrity.keys(aesKey);
                AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = new AesCbcWithIntegrity.CipherTextIvMac(cipherTextString);
                plainText = AesCbcWithIntegrity.decryptString(cipherTextIvMac, keys);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return plainText;
        } else {
            return cipherTextString;
        }
    }
}
