package com.summitconnect.biscuitrun.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

/**
 * Created by jess on 20/05/2017.
 */

public final class JSONUtility {
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create();

    private JSONUtility() {
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    public static <T> T fromJsonList(String json, Type listType){
        return new Gson().fromJson(json, listType);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    public static JsonObject getAsJSONObject(String jsonString){
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(jsonString).getAsJsonObject();
        return o;
    }
    //converts POJO to JSON element
    public static JsonElement parseToJson(Object object){
        JsonParser jsonParser = new JsonParser();
        return jsonParser.parse(toJson(object));
    }

    public static String demoJSON(){
        return "{\"1\":{\"title\":\"January 1 - 31 (DEMO)\",\"min_timestamp\":1493568000,\"max_timestamp\":1496246399,\"posts\":[{\"name\":\"Jay Ravida\",\"score\":\"485\"},{\"name\":\"Jess Villafuerte\",\"score\":\"428\"},{\"name\":\"Ian Dacanay\",\"score\":\"371\"},{\"name\":\"ichan dc\",\"score\":\"332\"},{\"name\":\"Richeel Valencia\",\"score\":\"324\"},{\"name\":\"Eunice\",\"score\":\"297\"},{\"name\":\"Mark Anthony Soriano\",\"score\":\"296\"},{\"name\":\"JM\",\"score\":\"280\"},{\"name\":\"test user1\",\"score\":\"279\"},{\"name\":\"Cedric Reyes\",\"score\":\"277\"}]}, \"2\":{\"title\":\"May 1 - 31 (DEMO)\",\"min_timestamp\":1493568000,\"max_timestamp\":1496246399,\"posts\":[{\"name\":\"Paolo Ravida\",\"score\":\"485\"},{\"name\":\"Karen Villafuerte\",\"score\":\"428\"},{\"name\":\"Jon Dacanay\",\"score\":\"371\"},{\"name\":\"ichan dc\",\"score\":\"332\"},{\"name\":\"Richeel Valencia\",\"score\":\"324\"},{\"name\":\"Eunice\",\"score\":\"297\"},{\"name\":\"Mark Anthony Soriano\",\"score\":\"296\"},{\"name\":\"JM\",\"score\":\"280\"},{\"name\":\"test user1\",\"score\":\"279\"},{\"name\":\"Cedric Reyes\",\"score\":\"277\"}]}}";
    }
}
