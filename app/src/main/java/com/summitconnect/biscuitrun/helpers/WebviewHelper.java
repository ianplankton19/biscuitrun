package com.summitconnect.biscuitrun.helpers;

import android.webkit.WebSettings;
import android.webkit.WebView;

import com.summitconnect.biscuitrun.base.BaseActivity;


/**
 * Created by jayan on 5/21/2017.
 */

public class WebviewHelper {

    //Load url with chaching
    public static void loadUrl(BaseActivity activity, WebView webView, String url) {
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(activity.getCacheDir().getPath());
        webView.getSettings().setJavaScriptEnabled(true);
        if (activity.isConnected()){
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        }else{
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
        webView.loadUrl(url);
    }

    public static void loadUrl(BaseActivity activity, WebView webView, String url, boolean isOnline) {
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(activity.getCacheDir().getPath());
        webView.getSettings().setJavaScriptEnabled(true);
        if (isOnline && activity.isConnected()){
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        }else{
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
        webView.loadUrl(url);
    }

    public static void loadUrl(BaseActivity activity, WebView webView, String url, int initialScale) {
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(activity.getCacheDir().getPath());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setInitialScale(initialScale<=0?150:initialScale);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(url);
    }

}
