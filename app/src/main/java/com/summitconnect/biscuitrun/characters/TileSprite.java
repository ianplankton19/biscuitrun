package com.summitconnect.biscuitrun.characters;

import com.badlogic.gdx.physics.box2d.Body;
import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


/**
 * Created by IanBlanco on 5/13/2017.
 */

public class TileSprite extends Sprite {

    private PhysicsHandler mPhysicsHandler;
    private ITextureRegion mITextureRegion;
    private boolean callBackChecker;


    public Body tileBody;
    private PhysicsWorld mPhysicsWorld;
    private OnGameActivity mOnGameActivity;

    private float mVelocityX;

    public TileSprite(float pX, float pY, ITextureRegion pTextureRegion,
                      VertexBufferObjectManager pVertexBufferObjectManager, PhysicsWorld physicsWorld,
                      OnGameActivity onGameActivity, float stageVelocity) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);

        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
        this.mOnGameActivity = onGameActivity;
        this.mVelocityX = stageVelocity;
        this.mPhysicsHandler.setVelocityX(-mVelocityX);
        this.callBackChecker = false;
        this.mPhysicsWorld = physicsWorld;
//        final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 0.0f, 0.3f);
//
//        tileBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this, BodyDef.BodyType.StaticBody, objectFixtureDef);
//        tileBody.setUserData("tiles");
//        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this, tileBody, true, false));
//        tileBody.setLinearVelocity(new Vector2( -100.5f, tileBody.getLinearVelocity().y));
//        tileBody.applyForce(new Vector2( -100.5f, tileBody.getLinearVelocity().y), new Vector2());
//        this.mPhysicsWorld.setVelocityIterations(-DEMO_VELOCITY);
//        this.mScene.registerUpdateHandler(mPhysicsWorld);

    }


    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

//        if (mOnGameActivity.mSuperRunActivated) {
//            this.mPhysicsHandler.setVelocityX(-mVelocityX - 100);
//            return;
//        }
//        this.mPhysicsHandler.setVelocityX(-mVelocityX);


    }
}
