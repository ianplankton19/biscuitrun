package com.summitconnect.biscuitrun.characters;

import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/18/2017.
 */

public class PowerUps extends AnimatedSprite{

    public int mValue;
    public PhysicsHandler mPhysicsHandler;

    public boolean hasCollide = false;

    private float mVelocityX;

    private OnGameActivity mOnGameActivity;

    public PowerUps(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
                    VertexBufferObjectManager pVertexBufferObjectManager, int value,
                    OnGameActivity onGameActivity, float stageVelocity) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);

        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
        this.mVelocityX = stageVelocity;
        this.mPhysicsHandler.setVelocityX(-mVelocityX);
        this.mOnGameActivity = onGameActivity;
        this.mValue = value;

        this.setCurrentTileIndex(value);
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        if (mOnGameActivity.mSuperRunActivated) {
            this.mPhysicsHandler.setVelocityX(-mVelocityX - 100);
            return;
        }
        this.mPhysicsHandler.setVelocityX(-mVelocityX);


    }
}
