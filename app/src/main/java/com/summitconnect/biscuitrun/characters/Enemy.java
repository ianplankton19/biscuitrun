package com.summitconnect.biscuitrun.characters;

import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/17/2017.
 */

public class Enemy extends AnimatedSprite {

    private PhysicsHandler mPhysicsHandler;
    public int mLevel;
    public boolean mEnemyIsCollide;
    public boolean mEnemyRemoveToArray = false;

    private float mVelocityX;
    private OnGameActivity mOnGameActivity;

    public Enemy(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
                 int level, VertexBufferObjectManager pVertexBufferObjectManager,
            OnGameActivity onGameActivity, float stageVelocity) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.animate(150);
        this.mLevel = level;
        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);

//        this.mPhysicsWorld.setVelocityIterations(-DEMO_VELOCITY);
        this.mVelocityX = stageVelocity;
        this.mPhysicsHandler.setVelocityX(-this.mVelocityX);
        this.mOnGameActivity = onGameActivity;
//        this.mAmmoArrayList
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

//        if (mOnGameActivity.mSuperRunActivated) {
//            this.mPhysicsHandler.setVelocityX(-mVelocityX - 100);
//            return;
//        }
        this.mPhysicsHandler.setVelocityX(-mVelocityX);


    }
}
