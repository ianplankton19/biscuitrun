package com.summitconnect.biscuitrun.characters;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.Body;
import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/12/2017.
 */

public class Player extends AnimatedSprite {

    public interface Callback {

        void characterOnChocoKnotChosen();
    }

    private Body playerBody;
    private float DEMO_VELOCITY = 400;
    public int JUMP_VELOCITY = 300;
    private Callback mCallback;
    public PhysicsWorld mPhysicsWorld;
    public BitmapTextureAtlas mBitmapTextureAtlas;
    public PhysicsHandler mPhysicsHandler;
    private OnGameActivity mOnGameActivity;
    public boolean isJump = false;
    public boolean isLand = false;

    //    Player behaviour
    public boolean onJump = false;
    public boolean onDead = false;
    public boolean onGround = false;

    public float yGroundJump;




    public Player(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
                      VertexBufferObjectManager pVertexBufferObjectManager, int spritePosition, Callback callback) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setCurrentTileIndex(spritePosition);
        this.mCallback = callback;
    }

    public Player(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
                      VertexBufferObjectManager pVertexBufferObjectManager, int spritePosition,
                  PhysicsWorld physicsWorld, BitmapTextureAtlas bitmapTextureAtlas, OnGameActivity onGameActivity) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager );
        this.animate(new long[] {150, 150, 150, 150}, 0, 3, true);
        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
        this.mOnGameActivity = onGameActivity;
        this.mBitmapTextureAtlas = bitmapTextureAtlas;

    }

    public void jump() {
//        if(isLanded) {
        Log.i("tag", "jumped");
//        playerBody.setLinearVelocity(new Vector2(playerBody.getLinearVelocity().x, -10.5f));
//        playerBody.setFixedRotation(true);
//        Debug.d("Jump");
//        }
        if (!onDead && !onJump && onGround) {
            yGroundJump = this.mY;
            this.mPhysicsHandler.setVelocityY(-DEMO_VELOCITY);
//            this.mOnGameActivity.
//            this.
            Log.i("tag", "jumped : cccccccccccccccccccccccc");
            isJump = true;
            onJump = true;
        }
    }

    private void landing() {

        Log.i("tag", "pababa");

//        this.mPhysicsHandler.setVelocityY(DEMO_VELOCITY);
        jumpVelocity();
        isLand = true;
        onJump = true;

    }

    private void jumpVelocity() {
        if (this.mY >= yGroundJump -170) {
                this.mPhysicsHandler.setVelocityY(100);
        }else {
                this.mPhysicsHandler.setVelocityY(DEMO_VELOCITY);
        }

    }


    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        playerAtlasUpdate();

        if (isJump) {
            if (this.mY <= yGroundJump - 170) {
                if (mPhysicsHandler != null) {
                        this.mPhysicsHandler.setVelocityY(-100);
                    if (this.mY <= yGroundJump -200) {
                        landing();
                        isJump = false;
                    }
                }
            }
        }
        if (isLand) {
//            if (this.mY >= 420 && !onGround) {
//                this.mPhysicsHandler.setVelocityY(0);
//                Log.i("huminto", "res: bbbbbbbbbbbbbbbbbbbbbbbb" + this.mY);
////                onJump = false;
//                isLand = false;
//                onJump = false;
//            }
        }
    }

    //    @Override
//    public boolean onAreaTouched() {
//        onChoose();
//        mCallback.characterOnCookieChosen();
//        return super.onAreaTouched();
//    }
    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        onChoose();
        mCallback.characterOnChocoKnotChosen();
        return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
    }

    public void onChoose() {
        this.animate(new long[] { 150, 150, 150, 150}, 0, 3, true);

    }


    public void onDeselect() {
        this.stopAnimation();
    }

    private void playerAtlasUpdate() {
        if (mOnGameActivity != null) {
            switch (mOnGameActivity.mIsSelected) {

                case 1 :
                    if (!this.onJump) {
                        this.mBitmapTextureAtlas.clearTextureAtlasSources();
                        BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_cookie_running_powerups.png" : "character_cookie_running.png", 0, 0, 4, 1);
                        return;
                    }
                    this.mBitmapTextureAtlas.clearTextureAtlasSources();
                    BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_cookie_jump_powerups.png" : "character_cookie_jump.png", 0, 0, 4, 1);

                    break;

                case 2 :
                    if (!this.onJump) {
                        this.mBitmapTextureAtlas.clearTextureAtlasSources();
                        BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_biscuit_running_powerups.png" : "character_biscuit_running.png", 0, 0, 4, 1);
                        return;
                    }
                    this.mBitmapTextureAtlas.clearTextureAtlasSources();
                    BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_biscuit_jump_powerups.png" : "character_biscuit_jump.png", 0, 0, 4, 1);

                    break;

                case 3 :
                    if (!this.onJump) {
                        this.mBitmapTextureAtlas.clearTextureAtlasSources();
                        BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_pretzel_running_powerups.png" : "character_pretzel_running.png", 0, 0, 2, 1);
                        return;
                    }
                    this.mBitmapTextureAtlas.clearTextureAtlasSources();
                    BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this.mOnGameActivity, mOnGameActivity.mSuperRunActivated ? "character_pretzel_jump_powerups.png" : "character_pretzel_jump.png", 0, 0, 2, 1);

                    break;
            }

        }

    }




}
