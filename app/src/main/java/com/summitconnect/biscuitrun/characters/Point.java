package com.summitconnect.biscuitrun.characters;

import com.summitconnect.biscuitrun.ongame.OnGameActivity;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/16/2017.
 */

public class Point extends Sprite{

    private static final int CAMERA_WIDTH = 1136;
    private static final int CAMERA_HEIGHT = 640;

    public PhysicsHandler mPhysicsHandler;
    public boolean displayScoreOnce = false;
    public Scene mScene;
    private float mVelocityX;

    public OnGameActivity mOnGameActivity;

    public Point(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,

                 Scene scene, OnGameActivity onGameActivity, float stageVelocity) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);

        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
//        this.mPhysicsWorld.setVelocityIterations(-DEMO_VELOCITY);
        this.mVelocityX = stageVelocity;
        this.mPhysicsHandler.setVelocityX(-mVelocityX);
        this.mOnGameActivity = onGameActivity;
        this.mScene = scene;

    }

    public void displayScore() {
        if (!displayScoreOnce) {

            mScene.detachChild(this);
            displayScoreOnce = true;
        }

    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        if (this.mOnGameActivity.mMagnetActivated) {
            if (this.mX < CAMERA_WIDTH / 2) {
                this.mPhysicsHandler.setVelocity(-2000, (this.mOnGameActivity.mPlayer.getY() - this.mY) * 4);
                return;
            }
        }
//        if (mOnGameActivity.mSuperRunActivated) {
//            this.mPhysicsHandler.setVelocityX(-mVelocityX - 100);
//            return;
//        }
        this.mPhysicsHandler.setVelocityX(-mVelocityX);



    }
}
