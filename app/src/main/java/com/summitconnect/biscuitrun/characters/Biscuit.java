package com.summitconnect.biscuitrun.characters;

import android.util.Log;

import com.badlogic.gdx.physics.box2d.Body;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/12/2017.
 */

public class Biscuit extends AnimatedSprite {


    public interface Callback {

        void characterBiscuitChosen();
    }

    private int DEMO_VELOCITY = 400;
    public int JUMP_VELOCITY = 300;
    private Callback mCallback;
    public PhysicsWorld mPhysicsWorld;
    private Body playerBody;
    public PhysicsHandler mPhysicsHandler;

    public boolean isJump = false;
    public boolean isLand = false;

    //    Player behaviour
    public boolean onJump = false;
    public boolean onDead = false;
    public boolean onGround = false;

    public float yGroundJump;


    public Biscuit(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, int spritePosition, Callback callback) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setCurrentTileIndex(spritePosition);
        mCallback = callback;
    }

    public Biscuit(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
                      VertexBufferObjectManager pVertexBufferObjectManager, int spritePosition, PhysicsWorld physicsWorld) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.animate(new long[] { 150, 150}, 0, 1, true);
        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
//        final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 0.0f, 0.5f);
//
//        this.mPhysicsWorld = physicsWorld;
////        this.mPhysicsWorld.setContactListener(createContactListener());
//
//        playerBody = PhysicsFactory.createCircleBody(this.mPhysicsWorld, this, BodyDef.BodyType.DynamicBody, objectFixtureDef);
//        playerBody.setUserData("player");
//        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this, playerBody, true, false));

//        mScene.registerUpdateHandler(this.mPhysicsWorld);
//        mPhysicsHandler.setVelocityY(DEMO_VELOCITY);
    }
    public void jump() {
//        if(isLanded) {
        Log.i("tag", "jumped");
//        playerBody.setLinearVelocity(new Vector2(playerBody.getLinearVelocity().x, -10.5f));
//        playerBody.setFixedRotation(true);
//        Debug.d("Jump");
//        }
        if (!onDead && !onJump && onGround) {
            yGroundJump = this.mY;
            this.mPhysicsHandler.setVelocityY(-JUMP_VELOCITY);
            Log.i("tag", "jumped : cccccccccccccccccccccccc");
            isJump = true;
            onJump = true;
        }
    }

    private void landing() {

        Log.i("tag", "pababa");

        this.mPhysicsHandler.setVelocityY(DEMO_VELOCITY);
        isLand = true;
        onJump = true;

    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);


        if (isJump) {
            if (this.mY <= yGroundJump - 200) {
                if (mPhysicsHandler != null) {
                    landing();
                    isJump = false;
                }
            }
        }
        if (isLand) {
//            if (this.mY >= 420 && !onGround) {
//                this.mPhysicsHandler.setVelocityY(0);
//                Log.i("huminto", "res: bbbbbbbbbbbbbbbbbbbbbbbb" + this.mY);
////                onJump = false;
//                isLand = false;
//                onJump = false;
//            }
        }
    }

    //    @Override
//    public boolean onAreaTouched() {
//        onChoose();
//        mCallback.characterOnCookieChosen();
//        return super.onAreaTouched();
//    }


    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        onChoose();
        return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
    }

    private void onChoose() {
        this.animate(new long[] { 150, 150, 150, 150}, 0, 3, true);
        mCallback.characterBiscuitChosen();
    }

    public void onDeselect() {
        this.stopAnimation();
    }
}
