package com.summitconnect.biscuitrun.characters;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/20/2017.
 */

public class Ammo extends Sprite{

    private float DEMO_VELOCITY = 500;
    private PhysicsHandler mPhysicsHandler;

    public Ammo(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);

        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);

        this.mPhysicsHandler.setVelocityX(DEMO_VELOCITY * 3);

    }
}
