package com.summitconnect.biscuitrun.characters;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by IanBlanco on 5/20/2017.
 */

public class Score extends AnimatedSprite {

    private PhysicsHandler mPhysicsHandler;
    private float yVal;
    private Scene mScene;
    private boolean toShow = false;

    public Score(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
                 int value, Scene scene) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setCurrentTileIndex(value);
        this.mPhysicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(this.mPhysicsHandler);
        this.mPhysicsHandler.setVelocityY(-100f);
        this.yVal = this.mY;

    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        if (this.mY < this.yVal - 100) {
//            setIgnoreUpdate(true);
//            self.clearUpdateHandlers();
            this.setCurrentTileIndex(0);
//            this.detachSelf();
        }

    }

}
