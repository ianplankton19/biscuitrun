package com.summitconnect.biscuitrun.enums;

/**
 * Created by jess on 21/05/2017.
 */

public enum MainMusicType {
    SPLASH, LOBBY, LVL_CLEAR, GAMEOVER, CONGRATULATIONS

}
