package com.summitconnect.biscuitrun.enums;

/**
 * Created by jess on 18/05/2017.
 */

public enum AudioControlMode {
    PLAY, PAUSE, STOP
}
