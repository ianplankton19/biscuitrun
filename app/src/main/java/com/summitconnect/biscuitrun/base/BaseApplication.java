package com.summitconnect.biscuitrun.base;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.summitconnect.biscuitrun.BuildConfig;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.helpers.encryption.EncryptionUtil;
import com.summitconnect.biscuitrun.singletons.RetrofitSingleton;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by jayan on 5/13/2017.
 */

public class BaseApplication extends Application{

    private static BaseApplication baseApplication = new BaseApplication();

    private static final int CONNECTION_TIME_OUT = 60;
    private static final int READ_TIME_OUT = 60;

    GoogleAnalytics googleAnalytics;
    Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize GoogleAnalytics
//        googleAnalytics = GoogleAnalytics.getInstance(this);
//        AnalyticsSingleton analyticsSingleton = AnalyticsSingleton.getInstance();
//        analyticsSingleton.initAnalytics(this);

        /** initialize Calligraphy */
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        /** initialize okhttp client */
        File cacheDir = getExternalCacheDir();
        if (cacheDir == null) {
            cacheDir = getCacheDir();
        }

        final Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        else
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        /** initialize ok http client */
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .cache(cache)
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .build();

        /** initialize retrofit */
        final RetrofitSingleton retrofitSingleton = RetrofitSingleton.getInstance();
            retrofitSingleton.initRetrofit(EncryptionUtil.decrypt(BuildConfig.AES, BuildConfig.BASE_URL), okHttpClient);
    }

    public static BaseApplication getInstance() {
        return baseApplication;
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = googleAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }
}
