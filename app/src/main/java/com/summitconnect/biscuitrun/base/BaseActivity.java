package com.summitconnect.biscuitrun.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.helpers.NetworkHelper;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by jayan on 5/17/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initProgressDialog();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) progressDialog.cancel();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    public boolean isConnected() {
        return NetworkHelper.isConnected(this);
    }

    public void showToast(int stringId) {
        if(toast != null){
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), getStringFromResource(stringId),Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showToast(String message) {
        if(toast != null){
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showLoading() {
        progressDialog.show();
    }

    public void dismissLoading() {
        progressDialog.hide();
    }

    public void setProgressDialogTitle(String title) {
        progressDialog.setTitle(title);
    }

    public void setProgressDialogMessage(String message) {
        progressDialog.setMessage(message);
    }

    public void setProgressDialogTitle(int stringId) {
        progressDialog.setTitle(getStringFromResource(stringId));
    }

    public void setProgressDialogMessage(int stringId) {
        progressDialog.setMessage(getStringFromResource(stringId));
    }

    private void initProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("");
        progressDialog.setMessage("Loading...");
        progressDialog.setProgress(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    public String getStringFromResource(int stringId){
        return getResources().getString(stringId);
    }

    public void showServerError() {
        showToast(R.string.all_message_error_servererror);
    }

    public void showFailedToConnectError() {
        showToast(R.string.all_message_error_unabletoconnect);
    }

    public void showSocketTimeoutError() {
        showToast(R.string.all_message_error_sockettimeout);
    }

    public void showNoConnectionError() {
        showToast(R.string.all_message_error_noconnection);
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public void goToOtherActivity(Class z){
        startActivity(new Intent(this,z));
        overridePendingTransition (R.anim.hold, R.anim.hold);
    }

    public Drawable setBackgroundImage(View view, int id){

        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(this, id));
        } else {
            view.setBackground(ContextCompat.getDrawable(this, id));
        }

        return ContextCompat.getDrawable(this, id);
    }

}
