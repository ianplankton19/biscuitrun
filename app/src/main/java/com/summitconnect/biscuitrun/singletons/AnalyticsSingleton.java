package com.summitconnect.biscuitrun.singletons;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.summitconnect.biscuitrun.base.BaseApplication;

/**
 * Created by jess on 22/05/2017.
 */

public class AnalyticsSingleton {

    private static AnalyticsSingleton analyticsSingleton = new AnalyticsSingleton();
    private BaseApplication application;
    private Tracker tracker;

    private AnalyticsSingleton(){}


    public void initAnalytics(BaseApplication application) {
        this.application = application;
        this.tracker = this.application.getDefaultTracker();
    }


    public void trackStartGame(){
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Page")
                .setAction("Event")
                .setLabel("Start Game")
                .setValue(1)
                .build());


    }

    public void trackRegister(){
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Page")
                .setAction("Submit")
                .setLabel("Register")
                .setValue(1)
                .build());
    }

    public void trackSubmitGameCode(){
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Page")
                .setAction("Submit")
                .setLabel("GameCode")
                .setValue(1)
                .build());
    }

    public void trackSelectCharacter(int characterId){
        String name = "";

        switch (characterId){
            case 1:
                name = "Cookies";
                break;
            case 2:
                name = "Biscuit";
                break;
            case 3:
                name = "Pretzel";
                break;
            default:
                break;
        }

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Player")
                .setAction("Choose")
                .setLabel(name)
                .setValue(1)
                .build());
    }

    public void trackReachedLevel(int level){
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Game")
                .setAction("Reached")
                .setLabel("Level " + level)
                .setValue(1)
                .build());
    }

    public void trackGameFinish(){
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Game")
                .setAction("Status")
                .setLabel("Game Finish")
                .setValue(1)
                .build());
    }

    public static AnalyticsSingleton getInstance(){
        return analyticsSingleton;
    }


}
