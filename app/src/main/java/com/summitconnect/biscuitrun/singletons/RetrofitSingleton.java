package com.summitconnect.biscuitrun.singletons;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.summitconnect.biscuitrun.data.api.ApiInterface;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jayan on 5/13/2017.
 */

public class RetrofitSingleton {

    private static RetrofitSingleton RETROFIT_SINGLETON = new RetrofitSingleton();
    private Retrofit retrofit;
    private ApiInterface apiInterface;


    private RetrofitSingleton() {

    }

    public static RetrofitSingleton getInstance() {
        return RETROFIT_SINGLETON;
    }

    public void initRetrofit(final String baseUrl, final OkHttpClient okHttpClient) {

        Gson GSON = new GsonBuilder()
                .serializeNulls()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        apiInterface = retrofit.create(ApiInterface.class);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }
}
