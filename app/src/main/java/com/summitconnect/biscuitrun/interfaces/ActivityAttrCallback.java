package com.summitconnect.biscuitrun.interfaces;

/**
 * Created by jess on 14/05/2017.
 */

public interface ActivityAttrCallback {
    boolean isHidden();
}
