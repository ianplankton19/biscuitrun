package com.summitconnect.biscuitrun.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jayan on 5/20/2017.
 */

@Data
@AllArgsConstructor
public class StartOnGameActivityEvent {
    int level;
    int selectedCharacter;
    int score;
    int generatedStar;
    boolean hasLife;
}
