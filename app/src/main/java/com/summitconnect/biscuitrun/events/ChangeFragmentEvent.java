package com.summitconnect.biscuitrun.events;

import android.support.v4.app.Fragment;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by jayan on 5/17/2017.
 */

@Data
@RequiredArgsConstructor
public class ChangeFragmentEvent {
    @NonNull
    Fragment fragment;
}
