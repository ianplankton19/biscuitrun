package com.summitconnect.biscuitrun.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jayan on 5/26/2017.
 */

@Data
@AllArgsConstructor
public class ExtraLifeEvent {
    boolean isExtraLife;
}
