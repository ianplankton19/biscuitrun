package com.summitconnect.biscuitrun.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jess on 18/05/2017.
 */
@Data
@AllArgsConstructor
public class AudioControlEvent {
    boolean isAudioOn;
}
