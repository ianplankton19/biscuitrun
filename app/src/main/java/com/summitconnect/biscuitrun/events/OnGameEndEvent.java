package com.summitconnect.biscuitrun.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jayan on 5/20/2017.
 */

@Data
@AllArgsConstructor
public class OnGameEndEvent {
    boolean isGameOver;
    boolean hasLife;
    int score;
    int starGeneratedCount;
    int levelFinished;
    int selectedCharacter;
}
