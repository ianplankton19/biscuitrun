package com.summitconnect.biscuitrun.ongame;

import android.graphics.Color;
import android.util.Log;

import com.summitconnect.biscuitrun.characters.PowerUps;
import com.summitconnect.biscuitrun.helpers.PositionHelper;
import com.summitconnect.biscuitrun.singletons.AnalyticsSingleton;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/**
 * Created by IanBlanco on 5/13/2017.
 */

public class OnGameExtension extends SimpleBaseGameActivity {

    protected static final int CAMERA_WIDTH = 1136;
    protected static final int CAMERA_HEIGHT = 640;

    protected String score = "0000";


    protected float STAGE_VELOCITY;
    protected float STAGE_VELOCITY_INCREMENT = 50;

    protected final Scene mScene = new Scene();

    protected Font font;

    protected BitmapTextureAtlas mTextureBackground;
    protected ITextureRegion mITextureRegionBackground;


    protected BitmapTextureAtlas mBitmapTextureAtlasTileSmall;
    protected ITextureRegion mITextureRegionTileSmall;

    protected BitmapTextureAtlas mBitmapTextureAtlasTileMedium;
    protected ITextureRegion mITextureRegionTileMedium;

    protected BitmapTextureAtlas mBitmapTextureAtlasTileLarge;
    protected ITextureRegion mITextureRegionTileLarge;

    protected BitmapTextureAtlas mBitmapTextureAtlasStar;
    protected ITextureRegion mITextureRegionStar;

    protected BitmapTextureAtlas mBitmapTextureAtlasShield;
    protected ITextureRegion mITextureRegionShield;

    protected BitmapTextureAtlas mBitmapTextureAtlasAmmo;
    protected ITextureRegion mITextureRegionAmmo;

    protected BitmapTextureAtlas mBitmapTextureAtlasGun;
    protected ITextureRegion mITextureRegionGun;

    protected BitmapTextureAtlas mBitmapTextureAtlasTimeBar;
    protected TiledTextureRegion mBitmapTextureRegionTimeBar;


    protected static BitmapTextureAtlas mBitmapTextureAtlasChocoKnot;
    protected static TiledTextureRegion mFaceTextureRegionChocoKnot;
    protected static BitmapTextureAtlas mBitmapTextureAtlasCookie;
    protected static TiledTextureRegion mFaceTextureRegionCookie;
    protected static BitmapTextureAtlas mBitmapTextureAtlasBiscuit;
    protected static TiledTextureRegion mFaceTextureRegionBiscuit;

    protected static BitmapTextureAtlas mBitmapTextureAtlasScore;
    protected static TiledTextureRegion mFaceTextureRegionScore;

    protected static BitmapTextureAtlas mBitmapTextureAtlasEnemyOne;
    protected static BitmapTextureAtlas mBitmapTextureAtlasEnemyTwo;
    protected static BitmapTextureAtlas mBitmapTextureAtlasEnemyThree;

    protected static TiledTextureRegion mFaceTextureRegionEnemyOne;
    protected static TiledTextureRegion mFaceTextureRegionEnemyTwo;
    protected static TiledTextureRegion mFaceTextureRegionEnemyThree;


    protected static BitmapTextureAtlas mBitmapTextureAtlasPowerUpsName;
    protected static TiledTextureRegion mFaceTextureRegionPowerUpsName;

    protected static BitmapTextureAtlas mBitmapTextureAtlasFunctionButton;
    protected static TiledTextureRegion mFaceTextureRegionFunctionButton;
    protected AnimatedSprite mButtonSprite;

    protected static BitmapTextureAtlas mBitmapTextureAtlasPowerUps;
    protected static TiledTextureRegion mFaceTextureRegionPowerUps;

    protected BitmapTextureAtlas hudTextureAtlas;
    protected ITextureRegion hudTextureRegion;
    protected Text hudScoreText;


    protected static BitmapTextureAtlas mBitmapTextureAtlasOne;
    protected ITextureRegion mITextureRegionOne;
    protected Sprite mLayerSprite;

    protected AnimatedSprite mTimeBar;

    protected boolean mShieldProvide = false;
    protected Sprite mShield;

    protected AnimatedSprite mPoweUpsName;

    protected boolean mGunProvide = false;
    protected Sprite mGun;

    private FixedStepEngine mFixedStepEngine;

    protected int mTimeElapsed;
    private int mTimeElapsedUps;

    protected PowerUps mPowerUps;
    protected boolean mReleaseUps = false;

    protected int mPowerUpsCounter;
    protected boolean mPowerUpsActivated = false;
    protected int mMyStage;

    private boolean isScoreTextAttach;

    //Music and Sound Effects
    protected Music backgroundMusic;
    protected Sound powerUpSound, gunSound;

    protected boolean mButtonIsTouched = false;

    protected void detectStage(int stage) {
        this.mMyStage = stage;
        Log.i("stage", "stage" + mMyStage);

    }


    @Override
    protected void onCreateResources() {

        loadFonts();

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        MusicFactory.setAssetBasePath("mfx/");
        SoundFactory.setAssetBasePath("mfx/");

        //Sound
        try {
            this.powerUpSound = SoundFactory.createSoundFromAsset(getSoundManager(), this, "sfx_powerup.m4a");
            this.gunSound = SoundFactory.createSoundFromAsset(getSoundManager(), this, "sfx_powerup_shooter.m4a");
            this.gunSound.setLooping(true);
        } catch (final IOException e) {
            Debug.e(e);
        }


/**
 * Background Declaration
 */
        switch (mMyStage) {
            case 1:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "background_one_space.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music1_universe.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(1);

                break;

            case 2:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_two.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music2_clouds.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(2);

                break;

            case 3:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_three.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music3_city.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(3);

                break;

            case 4:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_four.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music4_clouds.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(4);

                break;

            case 5:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_five.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music5_home.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(5);

                break;

            case 6:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_six.png", 0, 0);

                try {
                    this.backgroundMusic = MusicFactory.createMusicFromAsset(getMusicManager(), this, "music6_supermarket.m4a");
                    this.backgroundMusic.setLooping(true);
                } catch (final IOException e) {
                    Debug.e(e);
                }

                AnalyticsSingleton.getInstance().trackReachedLevel(6);

                break;
            default:
                this.mTextureBackground = new BitmapTextureAtlas(this.getTextureManager(), CAMERA_WIDTH, CAMERA_HEIGHT);
                this.mITextureRegionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTextureBackground,
                        this, "tiled_background_two.png", 0, 0);
                break;
        }

        mTextureBackground.load();

/**
 * Sprite Declaration
 */

        this.mBitmapTextureAtlasOne = new BitmapTextureAtlas(this.getTextureManager(), 1136, 640);
        this.mITextureRegionOne = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasOne,
                this, "clear_bg.png", 0, 0);
        this.mBitmapTextureAtlasOne.load();

        this.mBitmapTextureAtlasTileSmall = new BitmapTextureAtlas(this.getTextureManager(), 765, 87);
        this.mITextureRegionTileSmall = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasTileSmall,
                this, "tile_one_small.png", 0, 0);
        mBitmapTextureAtlasTileSmall.load();

        this.mBitmapTextureAtlasTileMedium = new BitmapTextureAtlas(this.getTextureManager(), 1017, 87);
        this.mITextureRegionTileMedium = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasTileMedium,
                this, "tile_one_medium.png", 0, 0);
        mBitmapTextureAtlasTileMedium.load();

        this.mBitmapTextureAtlasTileLarge = new BitmapTextureAtlas(this.getTextureManager(), 1530, 87);
        this.mITextureRegionTileLarge = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasTileLarge,
                this, "tile_one_large.png", 0, 0);
        mBitmapTextureAtlasTileLarge.load();

        this.mBitmapTextureAtlasStar = new BitmapTextureAtlas(this.getTextureManager(), 46, 42);
        this.mITextureRegionStar = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasStar,
                this, "ic_stars.png", 0, 0);
        this.mBitmapTextureAtlasStar.load();

        this.mBitmapTextureAtlasShield = new BitmapTextureAtlas(this.getTextureManager(), 166, 171);
        this.mITextureRegionShield = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasShield,
                this, "shield_sprite.png", 0, 0);
        this.mBitmapTextureAtlasShield.load();

        this.mBitmapTextureAtlasGun = new BitmapTextureAtlas(this.getTextureManager(), 53, 51, TextureOptions.BILINEAR);
        this.mITextureRegionGun = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasGun,
                this, "player_gun_sprite.png", 0, 0);
        this.mBitmapTextureAtlasGun.load();

        this.mBitmapTextureAtlasAmmo = new BitmapTextureAtlas(this.getTextureManager(), 49, 13);
        this.mITextureRegionAmmo = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlasAmmo,
                this, "player_gun_ammo.png", 0, 0);
        this.mBitmapTextureAtlasAmmo.load();

        this.mBitmapTextureAtlasTimeBar = new BitmapTextureAtlas(this.getTextureManager(), 527, 1260, TextureOptions.BILINEAR);
        this.mBitmapTextureRegionTimeBar = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasTimeBar,
                this, "timebar.png", 0, 0, 1, 21);
        this.mBitmapTextureAtlasTimeBar.load();

        this.hudTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 527, 1260, TextureOptions.BILINEAR);
        this.hudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.hudTextureAtlas, this, "hud_score-sheet0.png", 0, 0);
        this.hudTextureAtlas.load();

/**
 * TiledSprite Declaration
 */
        this.mBitmapTextureAtlasFunctionButton = new BitmapTextureAtlas(this.getTextureManager(), 128, 64, TextureOptions.BILINEAR);
        this.mFaceTextureRegionFunctionButton = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasFunctionButton,
                this, "play_button_sprite.png", 0, 0, 2, 1);
        this.mBitmapTextureAtlasFunctionButton.load();

        this.mBitmapTextureAtlasPowerUpsName = new BitmapTextureAtlas(this.getTextureManager(), 470, 394, TextureOptions.BILINEAR);
        this.mFaceTextureRegionPowerUpsName = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasPowerUpsName, this, "powerup_alert.png", 0, 0, 1, 6);
        this.mBitmapTextureAtlasPowerUpsName.load();

        this.mBitmapTextureAtlasChocoKnot = new BitmapTextureAtlas(this.getTextureManager(), 440, 100, TextureOptions.BILINEAR);
        this.mFaceTextureRegionChocoKnot = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasChocoKnot, this, "character_pretzel_running.png", 0, 0, 4, 1);
        this.mBitmapTextureAtlasChocoKnot.load();

        this.mBitmapTextureAtlasCookie = new BitmapTextureAtlas(this.getTextureManager(), 440, 100, TextureOptions.BILINEAR);
        this.mFaceTextureRegionCookie = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasCookie, this, "character_cookie_running.png", 0, 0, 4, 1);
        this.mBitmapTextureAtlasCookie.load();

        this.mBitmapTextureAtlasBiscuit = new BitmapTextureAtlas(this.getTextureManager(), 440, 100, TextureOptions.BILINEAR);
        this.mFaceTextureRegionBiscuit = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasBiscuit, this, "character_biscuit_running.png", 0, 0, 4, 1);
        this.mBitmapTextureAtlasBiscuit.load();

        this.mBitmapTextureAtlasEnemyOne = new BitmapTextureAtlas(this.getTextureManager(), 473, 237, TextureOptions.BILINEAR);
        this.mFaceTextureRegionEnemyOne = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasEnemyOne, this, "character_enemy_one.png", 0, 0, 4, 2);
        this.mBitmapTextureAtlasEnemyOne.load();

        this.mBitmapTextureAtlasScore = new BitmapTextureAtlas(this.getTextureManager(), 438, 58);
        this.mFaceTextureRegionScore = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasScore, this, "point_sprite.png", 0, 0, 6, 1);
        this.mBitmapTextureAtlasScore.load();


        this.mBitmapTextureAtlasEnemyTwo = new BitmapTextureAtlas(this.getTextureManager(), 473, 237, TextureOptions.BILINEAR);
        this.mFaceTextureRegionEnemyTwo = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasEnemyTwo, this, "character_enemy_two.png", 0, 0, 4, 2);
        this.mBitmapTextureAtlasEnemyTwo.load();

        this.mBitmapTextureAtlasEnemyThree = new BitmapTextureAtlas(this.getTextureManager(), 473, 237, TextureOptions.BILINEAR);
        this.mFaceTextureRegionEnemyThree = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasEnemyThree, this, "character_enemy_three.png", 0, 0, 4, 2);
        this.mBitmapTextureAtlasEnemyThree.load();


        this.mBitmapTextureAtlasPowerUps = new BitmapTextureAtlas(this.getTextureManager(), 162, 108, TextureOptions.DEFAULT);
        this.mFaceTextureRegionPowerUps = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlasPowerUps, this, "power_ups_sprite.png", 0, 0, 3, 2);
        this.mBitmapTextureAtlasPowerUps.load();

    }

    @Override
    protected Scene onCreateScene() {

        STAGE_VELOCITY = 350 + STAGE_VELOCITY_INCREMENT * mMyStage;

        this.mEngine.registerUpdateHandler(new FPSLogger());
//        this.mFixedStepEngine = new FixedStepEngine(this.mEngine.getEngineOptions(), 2);
        final AutoParallaxBackground autoParallaxBackground = new AutoParallaxBackground(0, 0, 0, 5);
        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(-30.0f, new Sprite(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, this.mITextureRegionBackground, this.getVertexBufferObjectManager())));

        mScene.setIgnoreUpdate(true);
        mScene.setBackground(autoParallaxBackground);

        mLayerSprite = new Sprite(0, 0, mITextureRegionOne, this.getVertexBufferObjectManager());
        mButtonSprite = new AnimatedSprite(1021, 27, mFaceTextureRegionFunctionButton, this.getVertexBufferObjectManager()) {

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

                if (pSceneTouchEvent.isActionDown()) {
                    mButtonIsTouched = true;
                }

                if (pSceneTouchEvent.isActionUp()) {
                    mButtonIsTouched = false;
                    if (!mScene.isIgnoreUpdate()) {
//                        mScene.setChildScene(mPauseScene, false, true, true);
                        mScene.setIgnoreUpdate(true);
                    } else {
//                        mScene.clearChildScene();
                        mScene.setIgnoreUpdate(false);
                    }
                    mButtonSprite.setCurrentTileIndex(mButtonSprite.getCurrentTileIndex() == 0 ? 1 : 0);

                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);

            }
        };
        mButtonSprite.setCurrentTileIndex(0);

        Sprite hudSprite = new Sprite(35, 10, hudTextureRegion, this.getVertexBufferObjectManager());
        hudScoreText = new Text(0, 0, this.font, score, this.getVertexBufferObjectManager());

        hudScoreText.setPosition(PositionHelper.centerXparent(hudSprite, hudScoreText) + 10, PositionHelper.centerYparent(hudSprite, hudScoreText));

        mScene.attachChild(mLayerSprite);
        mScene.attachChild(hudSprite);
        mScene.attachChild(hudScoreText);
        mScene.attachChild(mButtonSprite);

        isScoreTextAttach = true;

//        this.hudScoreText.setText(String.format("0020"));

        mScene.registerTouchArea(mButtonSprite);
//        mPauseScene.attachChild(mButtonSprite);
        mScene.setTouchAreaBindingOnActionDownEnabled(true);


        attachTimeBar();

        TimerHandler timerHandler = new TimerHandler(0.1f, true, new ITimerCallback() {
            int timeToDisplay;
            float i = 0;
            float s = 0;
            float upsCounter = 0;

            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {

                i = i + pTimerHandler.getTimerSeconds();
                s = s + pTimerHandler.getTimerSeconds();
                if ((int) i % 3 == 0) {
                    if (mTimeElapsed < (int) i) {
                        mTimeElapsed = (int) i;
                        timeToDisplay++;
                        if (timeToDisplay <= 20) {
                            mTimeBar.setCurrentTileIndex(timeToDisplay);
                        }

                    }
                }

                if (!mPowerUpsActivated) {
                    if ((int) s % 2 == 0) {
                        if (mTimeElapsedUps < (int) s) {
                            mTimeElapsedUps = (int) s;
                            mReleaseUps = true;
                        }
                    }
                }


                if (mPowerUpsActivated) {

                    upsCounter = upsCounter + pTimerHandler.getTimerSeconds();

                    if (mPowerUpsCounter <= (int) upsCounter) {
                        mPowerUpsActivated = false;
                        mPowerUpsCounter = 0;
                        upsCounter = 0;
                    }

                }


                Log.d("onTimePassed", "" + i);


            }
        });
        mScene.registerUpdateHandler(timerHandler);


//
        mEngine.registerUpdateHandler(new IUpdateHandler() {
            float i = 0;
            float sec = (float) 0.01;

            @Override
            public void onUpdate(float pSecondsElapsed) {


            }

            @Override
            public void reset() {

            }
        });

//        mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() {
//            @Override
//            public void onTimePassed(TimerHandler pTimerHandler) {
//
//                Log.d("onTimePassed", ""+pTimerHandler.getTimerSeconds());
//
//            }
//        }));

        return null;
    }


    @Override
    public EngineOptions onCreateEngineOptions() {
        final Camera camera = new Camera(0, 0, this.CAMERA_WIDTH, this.CAMERA_HEIGHT);

        EngineOptions engineOptions = new EngineOptions(true,
                ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(),
                camera);
        engineOptions.getAudioOptions().setNeedsMusic(true);
        engineOptions.getAudioOptions().setNeedsSound(true);
        return engineOptions;
    }

    private void loadFonts() {
        FontFactory.setAssetBasePath("fonts/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(this.getFontManager(), mainFontTexture, this.getAssets(), "arial-rounded-mt-bold.ttf", 30, true, Color.WHITE, 2, Color.WHITE);
        font.load();
    }

    public void setHUDScoreDisplay(int score) {
        String textScore = String.format("%04d", score);
        if (isScoreTextAttach)
            this.hudScoreText.setText(textScore);
    }


    private void attachTimeBar() {

        mTimeBar = new AnimatedSprite(300, (CAMERA_HEIGHT - (mBitmapTextureAtlasTimeBar.getHeight() / 20) * 2), mBitmapTextureRegionTimeBar, this.getVertexBufferObjectManager());
        mScene.attachChild(mTimeBar);
        mTimeBar.setCurrentTileIndex(0);
    }

}
