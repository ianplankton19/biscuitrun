package com.summitconnect.biscuitrun.ongame;

import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.summitconnect.biscuitrun.characters.Ammo;
import com.summitconnect.biscuitrun.characters.Enemy;
import com.summitconnect.biscuitrun.characters.Player;
import com.summitconnect.biscuitrun.characters.Point;
import com.summitconnect.biscuitrun.characters.PowerUps;
import com.summitconnect.biscuitrun.characters.Score;
import com.summitconnect.biscuitrun.characters.TileSprite;
import com.summitconnect.biscuitrun.enums.GameSoundsMode;
import com.summitconnect.biscuitrun.events.AudioControlEvent;
import com.summitconnect.biscuitrun.events.ExitApp;
import com.summitconnect.biscuitrun.events.ExtraLifeEvent;
import com.summitconnect.biscuitrun.events.OnGameEndEvent;
import com.summitconnect.biscuitrun.events.OnGameStartEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by IanBlanco on 5/13/2017.
 */

public class OnGameActivity extends OnGameExtension implements IOnSceneTouchListener {


    private boolean doubleBackToExitPressedOnce = false;
    public int mIsSelected;
    public int mLevel;
    public int mGeneratedStar;
    public boolean mHasGameCode;
    public boolean mIsExraLife;

    private int mInterval = 3;

    public Player mPlayer;

    private ArrayList<TileSprite> mTileSprites;
    private ArrayList<Point> mStarPoints;
    private ArrayList<PowerUps> mPowerUpsArrayList;
    private ArrayList<Enemy> mEnemies;
    private ArrayList<Score> mScores;
    private ArrayList<Ammo> mAmmos;

    private PhysicsHandler mPhysicsHandler;
    private PhysicsWorld mPhysicsWorld;

    public boolean mIsCatchLife = false;

    public float floorYColide;
    //    background behaviour
    private boolean onCollideWithTile;

    private int mScorePoints = 1;

    public int mTotalScorePoints;

    private final int bulletInterval = 10;
    private int bulletIntervalIncrement;

    private boolean mUpsNameIsDisplay = false;


    public boolean mMagnetActivated = false;
    public boolean mSuperRunActivated = false;
    public boolean mGunnerActivated = false;
    private boolean detachEnemyOnce = false;

    private boolean mSceneDetacher = false;
    private boolean isAudioOn = true;

    private boolean mPlayerOnAttachFirst = false;

    @Override
    public void onGameCreated() {
        super.onGameCreated();
    }

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
//        mIsSelected = 2;
//        mLevel = 1;
//        mHasGameCode = false;
//        mTotalScorePoints = 0;
//        mGeneratedStar = 0;
//        mIsCatchLife = false;
//        detectStage(mLevel);
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        playBackgroundMusic();
        playSound(GameSoundsMode.GUN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        pauseBackgroundMusic();
        stopSound(GameSoundsMode.GUN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopBackgroundMusic();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onExtraLife(ExtraLifeEvent event) {
        mIsExraLife = event.isExtraLife();
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onGameStart(OnGameStartEvent event) {
        mIsSelected = event.getSelectedCharacter();
        mLevel = event.getLevel();
        mHasGameCode = event.isHasGameCode();
        mTotalScorePoints = event.getScore();
        mGeneratedStar = event.getGeneratedStar();
        mIsCatchLife = event.isHasLife();
        detectStage(mLevel);
        EventBus.getDefault().removeStickyEvent(event);

        LogHelper.log("OnGameStartEvent", event.toString());
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void audioControl(AudioControlEvent event) {
        isAudioOn = event.isAudioOn();
    }

    @Override
    protected Scene onCreateScene() {
        super.onCreateScene();

        mEngine.registerUpdateHandler(new IUpdateHandler() {

            @Override
            public void onUpdate(float pSecondsElapsed) {
                if (mTimeElapsed >= 60) {
                    if (!mSceneDetacher) {
                        mSceneDetacher = true;
                        EventBus.getDefault().postSticky(new OnGameEndEvent(false, mIsCatchLife,
                                mTotalScorePoints, mGeneratedStar, mLevel, mIsSelected));
                        finish();
                        mScene.dispose();
                    }
                } else {
                    if (mScene.isVisible()) {
                        updatingTiles();
                        detectPlayerOnGround();
                    }
                }

                setHUDScoreDisplay(mTotalScorePoints);

            }

            @Override
            public void reset() {

            }
        });

        this.mScene.setOnSceneTouchListener(this);

        mTileSprites = new ArrayList<>();
        mStarPoints = new ArrayList<>();
        mPowerUpsArrayList = new ArrayList<>();
        mEnemies = new ArrayList<>();
        mScores = new ArrayList<>();
        mAmmos = new ArrayList<>();

        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), false) {
            float i = 0;
            float sec = (float) 0.01;

            @Override
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);
                i = i + sec;

            }
        };


        attachTiles(mScene);


        switch (mIsSelected) {
            case 1:
                mPlayer = new Player(10, 415 - mBitmapTextureAtlasCookie.getHeight(), this.mFaceTextureRegionCookie, this.getVertexBufferObjectManager(), 1, mPhysicsWorld, this.mBitmapTextureAtlasCookie, this);
                break;
            case 2:
                mPlayer = new Player(10, 415 - mBitmapTextureAtlasCookie.getHeight(), this.mFaceTextureRegionBiscuit, this.getVertexBufferObjectManager(), 1, mPhysicsWorld, this.mBitmapTextureAtlasBiscuit, this);
                break;
            case 3:
                mPlayer = new Player(10, 415 - mBitmapTextureAtlasCookie.getHeight(), this.mFaceTextureRegionChocoKnot, this.getVertexBufferObjectManager(), 1, mPhysicsWorld, this.mBitmapTextureAtlasChocoKnot, this);

//                detectPlayerOnGround(mChocoKnots);
                break;
        }

        mScene.attachChild(mPlayer);

        //play background music
        playBackgroundMusic();
        if (mScene.isVisible()) {
            mScene.setIgnoreUpdate(false);
        }

        return mScene;
    }


    private void detectPlayerOnGround() {

        if (mPlayer.getY() > CAMERA_HEIGHT + mPlayer.getHeight()) {
            EventBus.getDefault().postSticky(new OnGameEndEvent(true, mIsCatchLife,
                    mTotalScorePoints, mGeneratedStar, mLevel, mIsSelected));
            finish();
        }
        if (!mPlayer.onDead) {

            TileSprite tileSpriteCollision = null;
            for (TileSprite tileSprite : mTileSprites) {
                if (tileSprite.collidesWith(mPlayer)) {
                    onCollideWithTile = true;

                    tileSpriteCollision = tileSprite;
                    floorYColide = tileSprite.getY();
                }
            }

            if (tileSpriteCollision != null) {
                if (mPlayer.getY() + tileSpriteCollision.getHeight() - 25 < tileSpriteCollision.getY()) {
                    if (!mPlayer.onGround) {
                        mPlayer.mPhysicsHandler.setVelocityY(0);
                        mPlayer.setY(tileSpriteCollision.getY() - mPlayer.getHeight());
//                            Log.i("huminto", "res: bbbbbbbbbbbbbbbbbbbbbbbb" + this.mY);
//                onJump = false;
                        mPlayer.isLand = false;
                        mPlayer.onJump = false;
                    }

                } else {
                    mPlayer.onDead = true;

                }


                if (!mPlayer.onJump && !mPlayer.onDead && !mPlayer.onGround) {
                    mPlayer.mPhysicsHandler.setVelocityY(0);
                    mPlayer.setY(tileSpriteCollision.getY() - mPlayer.getHeight());
                    mPlayer.onGround = true;
                    mPlayer.onJump = false;

                    return;

                }
                if (!mPlayer.onJump && mPlayer.onDead) {
                    mPlayer.onGround = false;
                    if (mPlayerOnAttachFirst) {
                        mPlayer.mPhysicsHandler.setVelocityY(400);
                    }
                    return;
                }
                return;
            }
            mPlayerOnAttachFirst = true;
            mPlayer.onGround = false;
            onCollideWithTile = false;
            if (!mPlayer.onJump) {
                if (mPlayerOnAttachFirst) {
                    mPlayer.mPhysicsHandler.setVelocityY(400);
                }
            }
        }


    }


    private void updatingTiles() {
        int tileIndex = 0;
        int tileTest = 0;
        TileSprite tileSpriteToRemove = null;
        boolean generateTile = false;
        if (mTileSprites.size() > 0) {
            for (TileSprite tileSprite : mTileSprites) {
                tileIndex = tileIndex++;
                if (tileSprite.getX() <= -tileSprite.getWidth()) {
                    tileTest = tileIndex;
                    generateTile = true;
                }
            }
        }

        if (mSuperRunActivated) {
            for (int i = 1; i < mTileSprites.size(); i++) {
                if (mTileSprites.get(i).getX() > CAMERA_WIDTH) {
                    mTileSprites.get(i).setY(440);
                    mTileSprites.get(i).setX(mTileSprites.get(i - 1).getX() + mTileSprites.get(i - 1).getWidth());
                }
            }
            if (!detachEnemyOnce) {
                detachEnemyOnce = true;
                for (Enemy enemy : mEnemies) {
                    if (enemy.getX() > CAMERA_WIDTH) {
                        enemy.detachSelf();
                    }
                }
            }
        }

        if (generateTile) {
//            try {
            boolean generateEnemy = false;
            int pointToCreate = 0;
            TileSprite tileToAdd;
            int starXStart = 0;
            if (!mSuperRunActivated) {
                switch (getRandomFloorWidth()) {
                    case 1:
                        //Large tile
                        pointToCreate = 5;
                        generateEnemy = true;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth() + 175 + starXStart, getRandomFloor(), mITextureRegionTileLarge, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    case 2:
                        //Medium Tile
                        pointToCreate = 4;
                        generateEnemy = true;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth() + 175 + starXStart, getRandomFloor(), mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    case 3:
                        //Small Tile
                        pointToCreate = 3;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth() + 175 + starXStart, getRandomFloor(), mITextureRegionTileSmall, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    default:
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth() + 175 + starXStart, getRandomFloor(), mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                }
            } else {
                switch (getRandomFloorWidth()) {
                    case 1:
                        generateEnemy = true;
                        pointToCreate = 5;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth(), 440, mITextureRegionTileLarge, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    case 2:
                        generateEnemy = true;
                        pointToCreate = 4;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth(), 440, mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    case 3:
                        pointToCreate = 3;
                        starXStart = getRandomInterval();
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth(), 440, mITextureRegionTileSmall, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                    default:
                        tileToAdd = new TileSprite(mTileSprites.get(mTileSprites.size() - 1).getX() + mTileSprites.get(mTileSprites.size() - 1).getWidth(), 440, mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
                        break;
                }
            }
            mTileSprites.get(tileTest).detachSelf();
            mTileSprites.remove(tileTest);
            mLayerSprite.attachChild(tileToAdd);
//            tileToAdd.generateTile();

            float starWidth = mITextureRegionStar.getWidth();
            int randomStarInterval = new Random().nextInt(2) + 1;
            for (int i = 0; i < pointToCreate; i++) {

                Point point = new Point(tileToAdd.getX() + new Random().nextInt((int) (tileToAdd.getWidth() / 2)) + starXStart + starWidth * (i + randomStarInterval),
                        tileToAdd.getY() - tileToAdd.getHeight() + pointYPosition(),
                        mITextureRegionStar, this.getVertexBufferObjectManager(), mScene, this, STAGE_VELOCITY);
                mScene.attachChild(point);
                mStarPoints.add(point);
                mGeneratedStar++;
            }
            mTileSprites.add(tileToAdd);

            if (mReleaseUps) {
                PowerUps powerUps = new PowerUps(mTileSprites.get(mTileSprites.size() - 1).getX() + getPowerUpRandomX((int) tileToAdd.getWidth()),
                        tileToAdd.getY() - tileToAdd.getHeight() + pointYPosition(),
                        mFaceTextureRegionPowerUps, this.getVertexBufferObjectManager(), randomPowerUps(), this, STAGE_VELOCITY);
                mScene.attachChild(powerUps);
                mPowerUpsArrayList.add(powerUps);
                mReleaseUps = false;

            }


            if (generateEnemy) {

                Enemy enemy = null;
                switch (getRandomEnemy()) {
                    case 1:
                        enemy = new Enemy(mStarPoints.get(mStarPoints.size() - 1).getX(),
                                tileToAdd.getY() - mBitmapTextureAtlasEnemyOne.getHeight() / 2, mFaceTextureRegionEnemyOne, 1, this.getVertexBufferObjectManager(), this, STAGE_VELOCITY);
                        break;
                    case 2:
                        enemy = new Enemy(mStarPoints.get(mStarPoints.size() - 1).getX(),
                                tileToAdd.getY() - mBitmapTextureAtlasEnemyTwo.getHeight() / 2, mFaceTextureRegionEnemyTwo, 2, this.getVertexBufferObjectManager(), this, STAGE_VELOCITY);
                        break;
                    case 3:
                        enemy = new Enemy(mStarPoints.get(mStarPoints.size() - 1).getX(),
                                tileToAdd.getY() - mBitmapTextureAtlasEnemyThree.getHeight() / 2, mFaceTextureRegionEnemyThree, 3, this.getVertexBufferObjectManager(), this, STAGE_VELOCITY);

                        break;

                }
                mEnemies.add(enemy);
                mScene.attachChild(enemy);
                generateEnemy = false;
            }
            generateTile = false;
        }

        for (Point point : mStarPoints) {
            if (point.getX() <= -point.getWidth()) {
                mScene.detachChild(point);
                point.unregisterUpdateHandler(point.mPhysicsHandler);
//                mStarPoints.remove(point);
            }

            if (mMagnetActivated) {
                if (point.getX() <= mPlayer.getX() + mPlayer.getWidth() - 20 && point.getX() > 0) {
                    if (!point.displayScoreOnce) {
                        Score score = new Score(point.getX(), point.getY() - mBitmapTextureAtlasScore.getHeight() / 2,
                                mFaceTextureRegionScore, this.getVertexBufferObjectManager(), mScorePoints, this.mScene);
                        score.setScale(.7f);
                        mScene.attachChild(score);

                        mTotalScorePoints = mTotalScorePoints + mScorePoints;
                        mScores.add(score);
                        point.displayScoreOnce = true;
                    }
                }
            }

            if (point.collidesWith(mPlayer)) {
//                if (!point.displayScore) {
//                    point.displayScore = true;
//                if (!mScoreLimiter) {
//                    Score score = new Score(point.getX(), point.getY(),
//                            mFaceTextureRegionScore, this.getVertexBufferObjectManager(), 0, mScene);
//                    mScene.attachChild(score);
//                    mScoreLimiter = true;
//                }
//                    mStarPoints.remove(point);

                if (!point.displayScoreOnce) {
                    Score score = new Score(point.getX(), point.getY() - mBitmapTextureAtlasScore.getHeight() / 2,
                            mFaceTextureRegionScore, this.getVertexBufferObjectManager(), mScorePoints, this.mScene);
                    score.setScale(.7f);
                    mScene.attachChild(score);

                    mTotalScorePoints = mTotalScorePoints + mScorePoints;
                    mScores.add(score);
                    point.displayScoreOnce = true;
                }
//                point.displayScore();
                mScene.detachChild(point);
                point.unregisterUpdateHandler(point.mPhysicsHandler);
//                }

            }

        }

        for (Score score : mScores) {
            if (score.getX() <= -score.getHeight()) {
                mScene.detachChild(score);
            }
        }

        for (Enemy enemy : mEnemies) {
            if (enemy.getX() <= -enemy.getWidth()) {
                enemy.mEnemyRemoveToArray = true;
                mScene.detachChild(enemy);
            }
            if (enemy.collidesWith(mPlayer)) {
                if (!enemy.mEnemyIsCollide) {
                    Score score = null;
                    if (!mShieldProvide) {
                        switch (enemy.mLevel) {

                            case 1:
                                mTotalScorePoints = mTotalScorePoints - 1;
                                score = new Score(enemy.getX(), enemy.getY() - mBitmapTextureAtlasScore.getHeight() / 2,
                                        mFaceTextureRegionScore, this.getVertexBufferObjectManager(), 3, this.mScene);
                                score.setScale(.7f);
                                break;

                            case 2:
                                mTotalScorePoints = mTotalScorePoints - 5;
                                score = new Score(enemy.getX(), enemy.getY() - mBitmapTextureAtlasScore.getHeight() / 2,
                                        mFaceTextureRegionScore, this.getVertexBufferObjectManager(), 4, this.mScene);
                                score.setScale(.7f);
                                break;

                            case 3:
                                score = new Score(enemy.getX(), enemy.getY() - mBitmapTextureAtlasScore.getHeight() / 2,
                                        mFaceTextureRegionScore, this.getVertexBufferObjectManager(), 5, this.mScene);
                                score.setScale(.7f);
                                mTotalScorePoints = mTotalScorePoints - 10;
                                break;
                        }
                        mScene.attachChild(score);
                    }
                    if (mTotalScorePoints <= 0) {
                        mTotalScorePoints = 0;
                    }

                    enemy.mEnemyIsCollide = true;
                }
                enemy.mEnemyRemoveToArray = true;
                mScene.detachChild(enemy);

            }
            if (enemy.getX() < CAMERA_WIDTH - enemy.getWidth()) {
                for (Ammo ammo : mAmmos) {
                    if (enemy.collidesWith(ammo)) {
                        enemy.mEnemyRemoveToArray = true;
                        enemy.detachSelf();
                    }
                }
            }

        }


        if (!mPowerUpsActivated) {
            clearUps();
        } else {
            for (PowerUps powerUps : mPowerUpsArrayList) {
                powerUps.detachSelf();
            }
            mPowerUpsArrayList.clear();
        }

//        if (mPowerUps != null) {
        for (PowerUps powerUps : mPowerUpsArrayList) {
            if (powerUps.getX() <= -powerUps.getWidth()) {
                mScene.detachChild(powerUps);
            }

            if (powerUps.collidesWith(mPlayer)) {
                if (!powerUps.hasCollide) {

                    mPowerUpsActivated = true;
                    getPowerUpsValue(powerUps.mValue);
                    powerUps.hasCollide = true;
                }
                mScene.detachChild(powerUps);

            }
        }

        if (mGunnerActivated) {
            if (mGun != null) {
                bulletIntervalIncrement++;

                if (bulletIntervalIncrement == bulletInterval) {
                    Ammo ammo = new Ammo(mPlayer.getX() + mPlayer.getWidth(),
                            mPlayer.getY() + mPlayer.getHeight() / 2, mITextureRegionAmmo, this.getVertexBufferObjectManager());
                    mScene.attachChild(ammo);
                    mAmmos.add(ammo);

                    bulletIntervalIncrement = 0;
                }
            }

        }

        for (Ammo ammo : mAmmos) {
            if (ammo.getX() >= CAMERA_WIDTH) {
                ammo.detachSelf();
            }

        }

    }


    private void clearUps() {

        if (mShield != null) {
            mPlayer.detachChild(mShield);
            mShieldProvide = false;
        }

        if (mGun != null) {
            mPlayer.detachChild(mGun);
            mGunProvide = false;
            stopSound(GameSoundsMode.GUN);
        }

        if (mPoweUpsName != null) {
            mPoweUpsName.detachSelf();
            mUpsNameIsDisplay = false;
        }

        mMagnetActivated = false;
        mGunnerActivated = false;
        mSuperRunActivated = false;
        detachEnemyOnce = false;

        mScorePoints = 1;
    }

    public void getPowerUpsValue(int powerUpsValue) {

        playSound(GameSoundsMode.POWERUPS);

        switch (powerUpsValue) {

            case 0: // Super Run
                mPowerUpsCounter = 10;
                mPowerUpsActivated = true;
                mSuperRunActivated = true;
                break;

            case 1: // Double Coin
                mPowerUpsCounter = 5;
                mPowerUpsActivated = true;
                mScorePoints = 2;
                break;

            case 2: // Life
                mPowerUpsCounter = 2;
                mPowerUpsActivated = true;
                mIsCatchLife = true;
                break;

            case 3: // Shield

                mPowerUpsCounter = 10;
                mPowerUpsActivated = true;
                if (!mShieldProvide) {
                    mShield = new Sprite(-20, -30,
                            mITextureRegionShield, this.getVertexBufferObjectManager());
//                    mShield.setZIndex(-100);
                    mPlayer.attachChild(mShield);
                    mShieldProvide = true;
                }
                break;

            case 4: //Magnet
                mPowerUpsCounter = 10;
                mPowerUpsActivated = true;
                mMagnetActivated = true;

                break;

            case 5: //gun
                mPowerUpsCounter = 10;
                mPowerUpsActivated = true;
                mGunnerActivated = true;
                if (!mGunProvide) {
                    mGun = new Sprite(68, 28,
                            mITextureRegionGun, this.getVertexBufferObjectManager());
//                    mShield.setZIndex(-100);
                    mPlayer.attachChild(mGun);
                    mGunProvide = true;
                }
                playSound(GameSoundsMode.GUN);
                break;
        }
        if (!mUpsNameIsDisplay) {
            mUpsNameIsDisplay = true;
            mPoweUpsName = new AnimatedSprite(0, 0, this.mFaceTextureRegionPowerUpsName, this.getVertexBufferObjectManager());
            mPoweUpsName.setPosition(25, CAMERA_HEIGHT - (mPoweUpsName.getHeight() + 25));
            mPoweUpsName.setCurrentTileIndex(powerUpsValue);
            mScene.attachChild(mPoweUpsName);
        }
    }

    public int getPowerUpRandomX(int tileWidth) {
        Random random = new Random();
        return random.nextInt(tileWidth);
    }

    public float pointYPosition() {
        Random rand = new Random();
        switch (rand.nextInt(3) + 1) {
            case 1:
                return -mFaceTextureRegionCookie.getHeight();
            case 2:
                return -mFaceTextureRegionCookie.getHeight() + (int) (mITextureRegionStar.getHeight() * 1.5);
            case 3:
                return -mFaceTextureRegionCookie.getHeight() + (int) (mITextureRegionStar.getHeight() * 2.5);
            default:
                return -mFaceTextureRegionCookie.getHeight();
        }
    }

    protected int randomPowerUps() {
        Random random = new Random();
        int[] powerUps;

        if (!mHasGameCode) {
            powerUps = new int[]{0, 1, 3, 5};
//            powerUps = new int[]{0, 1, 3, 4, 5};
            int x = random.nextInt(powerUps.length);
            return powerUps[x];
        } else {
            if (mIsCatchLife || mIsExraLife) {
                powerUps = new int[]{0, 1, 3, 4, 5};
            } else {
                powerUps = new int[]{0, 2, 1, 3, 4, 5};
            }
            int x = random.nextInt(powerUps.length);
            return powerUps[x];
        }
    }

    public int getRandomFloorWidth() {
        Random rand = new Random();
        int widthToReturn = 0;
        int randomized = rand.nextInt(100) + 1;
        if (mLevel == 1) {
            if (randomized <= 40) {
                widthToReturn = 1;
            } else if (randomized >= 41 && randomized <= 70) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 2 || mLevel == 3) {
            if (randomized <= 30) {
                widthToReturn = 1;
            } else if (randomized >= 31 && randomized <= 60) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 4 || mLevel == 5) {
            if (randomized <= 20) {
                widthToReturn = 1;
            } else if (randomized >= 21 && randomized <= 60) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 6) {
            if (randomized <= 40) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }

        return widthToReturn;
    }


    public int getRandomEnemy() {
        Random rand = new Random();
        int widthToReturn = 0;
        int randomized = rand.nextInt(100) + 1;
        if (mLevel == 1) {
            if (randomized <= 60) {
                widthToReturn = 1;
            } else if (randomized >= 61 && randomized <= 90) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 2 || mLevel == 3) {
            if (randomized <= 50) {
                widthToReturn = 1;
            } else if (randomized >= 51 && randomized <= 85) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 4 || mLevel == 5) {
            if (randomized <= 40) {
                widthToReturn = 1;
            } else if (randomized >= 41 && randomized <= 70) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }
        if (mLevel == 6) {
            if (randomized <= 30) {
                widthToReturn = 1;
            } else if (randomized >= 31 && randomized <= 75) {
                widthToReturn = 2;
            } else {
                widthToReturn = 3;
            }
        }

        return widthToReturn;
    }

    public int getRandomFloor() {

        Random rand = new Random();
        int i = rand.nextInt(3) + 1;
        switch (i) {
            case 1:
                return 390;
            case 2:
                return 340;
            case 3:
                return 440;
            default:
                return 390;
        }

    }


    public int getRandomInterval() {
        Random random = new Random();
        return random.nextInt(100) + 1;
    }

    private void attachTiles(Scene scene) {

        TileSprite tile1 = new TileSprite(0, 415, mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
        TileSprite tile2 = new TileSprite(0 + mITextureRegionTileMedium.getWidth() + 175 + getRandomInterval(), 415, mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);
        TileSprite tile3 = new TileSprite((0 + tile2.getX() + mITextureRegionTileMedium.getWidth() + 175 + getRandomInterval()), 415, mITextureRegionTileMedium, this.getVertexBufferObjectManager(), mPhysicsWorld, this, STAGE_VELOCITY);

        mTileSprites.add(tile1);
        mTileSprites.add(tile2);
        mTileSprites.add(tile3);

        scene.attachChild(tile1);
        scene.attachChild(tile2);
        scene.attachChild(tile3);
//        ArrayList<Point> pointToGroup = new ArrayList<>();
        for (int i = 1; i < mTileSprites.size(); i++) {
            int pointToCreate = 4;
            int randomStarInterval = new Random().nextInt(2) + 1;
            for (int s = 0; s < pointToCreate; s++) {
                Point point = new Point(mTileSprites.get(i).getX() + getRandomInterval() + mBitmapTextureAtlasStar.getWidth() * (s + randomStarInterval),
                        mTileSprites.get(i).getY() - mTileSprites.get(i).getHeight() + pointYPosition(),
                        mITextureRegionStar, this.getVertexBufferObjectManager(), scene, this, STAGE_VELOCITY);
                scene.attachChild(point);
                mStarPoints.add(point);
//                pointToGroup.add(point);
                mGeneratedStar++;
            }


//            final SpriteBatch spriteBatch = new DynamicSpriteBatch(this.mBitmapTextureAtlasStar, pointToGroup.size(), this.getVertexBufferObjectManager()) {
//                @Override
//                protected boolean onUpdateSpriteBatch() {
//
//                    for (Point point : pointToGroup) {
//                        this.draw(point);
//                    }
//                    return true;
//                }
//            };

//            spriteBatch.setPosition( CAMERA_WIDTH + 100 ,CAMERA_HEIGHT / 2);
//            mScene.attachChild(spriteBatch);
        }

    }


    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

        if (pSceneTouchEvent.isActionDown()) {
            if (!mButtonIsTouched) {
                mPlayer.jump();
            }
            return true;
        }
        return false;
    }

    private void playBackgroundMusic() {
        if (backgroundMusic != null && isAudioOn)
            backgroundMusic.play();
    }

    private void stopBackgroundMusic() {
        if (backgroundMusic != null)
            try {
                backgroundMusic.stop();
            } catch (Exception e) {
//                No Background music at all
            }
    }

    private void pauseBackgroundMusic() {
        if (backgroundMusic != null)
            backgroundMusic.pause();
    }

    private void playSound(GameSoundsMode soundsMode) {
        switch (soundsMode) {
            case POWERUPS:
                if (isAudioOn)
                    powerUpSound.play();
                break;

            case GUN:
                if (mGunnerActivated && isAudioOn) { //check because it need to play again in onResume if gun is activated
                    gunSound.setRate(2);
                    gunSound.play();
                }
                break;

            default:
                break;
        }
    }

    private void stopSound(GameSoundsMode soundsMode) {
        switch (soundsMode) {
            case POWERUPS:
                powerUpSound.stop();
                break;

            case GUN:
                if (mGunnerActivated) {
                    gunSound.stop();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            EventBus.getDefault().postSticky(new ExitApp());
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
