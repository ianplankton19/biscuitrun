package com.summitconnect.biscuitrun.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by jess on 20/05/2017.
 */
@Data
public class PostScore {

    @SerializedName("name")
    String name;
    @SerializedName("score")
    String score;
}
