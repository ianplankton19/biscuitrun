package com.summitconnect.biscuitrun.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by jayan on 5/13/2017.
 */

@Data
public class User {
    @SerializedName("user_id")
    String userId;
    @SerializedName("name")
    String name;
    @SerializedName("email")
    String email;
}
