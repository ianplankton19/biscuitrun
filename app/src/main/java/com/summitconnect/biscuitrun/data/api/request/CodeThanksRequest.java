package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jayan on 5/23/2017.
 */
@Data
@AllArgsConstructor
public class CodeThanksRequest extends BaseRequest{
    @SerializedName("userId")
    String userId;
}
