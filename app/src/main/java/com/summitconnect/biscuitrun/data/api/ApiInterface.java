package com.summitconnect.biscuitrun.data.api;


import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GameCodeResponse;
import com.summitconnect.biscuitrun.data.api.response.LoginResponse;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by jayan on 4/13/2017.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("response/user_app.php")
    Observable<BaseResponse> register(@Field("email") String email, @Field("mobile") String mobile,
                                      @Field("name") String name, @Field("password") String password,
                                      @Field("age") String age, @Field("address") String postalAddress,
                                      @Field("mode") String mode, @Field("app_platform") String appPlatform,
                                      @Field("app_version") String appVersion, @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/user_app.php")
    Observable<LoginResponse> login(@Field("email") String email, @Field("password") String password,
                                    @Field("mode") String mode, @Field("app_platform") String appPlatform,
                                    @Field("app_version") String appVersion, @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/forgotpw_app.php")
    Observable<BaseResponse> forgotPassword(@Field("email") String email, @Field("password") String password,
                                            @Field("app_platform") String appPlatform,
                                            @Field("app_version") String appVersion,
                                            @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/lastlog_app.php")
    Observable<BaseResponse> retrieveLastLog(@Field("user_id") String userId,@Field("app_platform") String appPlatform,
                                             @Field("app_version") String appVersion, @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/code_app.php")
    Observable<GameCodeResponse> addGameCode(@Field("storecode") String storeCode, @Field("month") String month,
                                             @Field("transid") String transId, @Field("year") String year,
                                             @Field("pos") String pos, @Field("day") String day,
                                             @Field("user_id") String userId, @Field("app_platform") String appPlatform,
                                             @Field("app_version") String appVersion, @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/codethanks_app.php")
    Observable<BaseResponse> codeThanks(@Field("user_id") String userId, @Field("app_platform") String appPlatform,
                                        @Field("app_version") String appVersion,
                                        @Field("app_key") String appKey);

    @FormUrlEncoded
    @POST("response/log_app.php")
    Observable<BaseResponse> log(@Field("user_id") String userId, @Field("email") String email,
                                 @Field("code") String code, @Field("score") String score,
                                 @Field("score_key") String scorekey,
                                 @Field("app_platform") String appPlatform,
                                 @Field("app_version") String appVersion, @Field("app_key") String appKey);

    @GET("response/leaderboard_app.php")
    Observable<Response<ResponseBody>> getHighScore();
}
