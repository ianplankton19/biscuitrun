package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.summitconnect.biscuitrun.BuildConfig;
import com.summitconnect.biscuitrun.helpers.encryption.EncryptionUtil;

/**
 * Created by jayan on 5/13/2017.
 */

public class BaseRequest {

    @SerializedName("appPlatform")
    private String appPlatform = BuildConfig.APP_PLATFORM;
    @SerializedName("appVersion")
    private String appVersion = String.valueOf(BuildConfig.VERSION_NAME);
    @SerializedName("appKey")
    private String appKey = EncryptionUtil.decrypt(BuildConfig.AES, BuildConfig.APP_KEY);

    public String getAppPlatform() {
        return appPlatform;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String getAppKey() {
        return appKey;
    }
}
