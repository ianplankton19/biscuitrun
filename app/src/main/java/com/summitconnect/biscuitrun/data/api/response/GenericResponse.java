package com.summitconnect.biscuitrun.data.api.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by jayan on 4/16/2017.
 */

@Data
public class GenericResponse {

    @SerializedName("message")
    private String message;
}