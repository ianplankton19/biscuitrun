package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jayan on 5/13/2017.
 */

public class ForgotPasswordRequest extends BaseRequest{

    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;


    public ForgotPasswordRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
