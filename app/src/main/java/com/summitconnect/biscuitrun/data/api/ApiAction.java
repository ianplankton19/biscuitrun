package com.summitconnect.biscuitrun.data.api;

/**
 * Created by jayan on 4/14/2017.
 */

public enum ApiAction {
    REGISTER,
    LOGIN,
    FORGOT_PASSWORD,
    LAST_LOG,
    ADD_GAMECODE,
    CODE_THANKS,
    LOG,
    HIGHSCORE
}
