package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jayan on 5/13/2017.
 */

public class LogRequest extends BaseRequest {

    @SerializedName("userId")
    String userId;
    @SerializedName("email")
    String email;
    @SerializedName("code")
    String code;
    @SerializedName("score")
    String score;

    public LogRequest(String userId, String email, String code, String score) {
        this.userId = userId;
        this.email = email;
        this.code = code;
        this.score = score;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
