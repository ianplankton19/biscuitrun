package com.summitconnect.biscuitrun.data.api;

import com.google.gson.Gson;
import com.summitconnect.biscuitrun.base.Sha1Generator;
import com.summitconnect.biscuitrun.data.api.request.ForgotPasswordRequest;
import com.summitconnect.biscuitrun.data.api.request.GameCodeRequest;
import com.summitconnect.biscuitrun.data.api.request.LastLogRequest;
import com.summitconnect.biscuitrun.data.api.request.LogRequest;
import com.summitconnect.biscuitrun.data.api.request.LoginRequest;
import com.summitconnect.biscuitrun.data.api.request.RegisterRequest;
import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GameCodeResponse;
import com.summitconnect.biscuitrun.data.api.response.LoginResponse;
import com.summitconnect.biscuitrun.helpers.LogHelper;
import com.summitconnect.biscuitrun.singletons.RetrofitSingleton;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.security.NoSuchAlgorithmException;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jayan on 4/14/2017.
 */

public class ApiRequestHelper {
    private OnApiRequestListener onApiRequestListener;
    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public ApiRequestHelper(OnApiRequestListener onApiRequestListener) {
        this.onApiRequestListener = onApiRequestListener;
        this.retrofit = RetrofitSingleton.getInstance().getRetrofit();
        this.apiInterface = RetrofitSingleton.getInstance().getApiInterface();;
    }

    /**
     * handle api result using lambda
     *
     * @param action     identification of the current api request
     * @param observable actual process of the api request
     */
    private void handleObservableResult(final ApiAction action, final Observable observable) {
        observable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(result -> onApiRequestListener.onApiRequestSuccess(action, result),
                        throwable -> onApiRequestListener.onApiRequestFailed(action, (Throwable) throwable),
                        () -> LogHelper.log("api", "api request completed --> " + action));
    }

    public <T> T parseError(final Class clazz, Response<?> response) {
        final Converter<ResponseBody, T> converter = retrofit.responseBodyConverter(clazz,
                new Annotation[0]);
        T error = null;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            e.printStackTrace();
            LogHelper.log("err", "error in mapping ---> " + e.getMessage());
        }
        return error;
    }

    public <T> T parseError(final Class clazz, final String json) {
        return (T) new Gson().fromJson(json, clazz);
    }

    /*****************APP SPECIFIC REQUESTS *****************/
    public void register(RegisterRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.REGISTER);

        final Observable<BaseResponse> observable = apiInterface.register(request.getEmail(),
                    request.getMobile(), request.getName(),request.getPassword(),request.getAge(),
                    request.getPostal(),request.getMode(), request.getAppPlatform(),
                request.getAppVersion(), generateKey(request.getAppKey() + request.getEmail()));
        handleObservableResult(ApiAction.REGISTER, observable);
    }

    public void login(LoginRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.LOGIN);
        final Observable<LoginResponse> observable = apiInterface.login(request.getEmail(),
                    request.getPassword(),request.getMode(),request.getAppPlatform(),
                    request.getAppVersion(), generateKey(request.getAppKey() + request.getEmail()));
        handleObservableResult(ApiAction.LOGIN, observable);
    }

    public void forgotPassword(ForgotPasswordRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.FORGOT_PASSWORD);
        final Observable<BaseResponse> observable = apiInterface.forgotPassword(request.getEmail(),
                    request.getPassword(),request.getAppPlatform(),
                    request.getAppVersion(), generateKey(request.getAppKey() + request.getEmail()));
        handleObservableResult(ApiAction.FORGOT_PASSWORD, observable);
    }

    public void lastLog(LastLogRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.LAST_LOG);
        final Observable<BaseResponse> observable = apiInterface.retrieveLastLog(request.getUserId(),request.getAppPlatform(),
                request.getAppVersion(), generateKey(request.getAppKey() + request.getUserId()));
        handleObservableResult(ApiAction.LAST_LOG, observable);
    }

    public void addGameCode(GameCodeRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.ADD_GAMECODE);
        final Observable<GameCodeResponse> observable = apiInterface.addGameCode(request.getStoreCode(),
                request.getMonth(), request.getTransId(),request.getYear(),request.getPos(),
                request.getDay(),request.getUserId(), request.getAppPlatform(),
                request.getAppVersion(), generateKey(request.getAppKey() + request.getUserId()));
        handleObservableResult(ApiAction.ADD_GAMECODE, observable);
    }

    public void codeThanks(LastLogRequest request) {
        onApiRequestListener.onApiRequestStart(ApiAction.CODE_THANKS);
        final Observable<BaseResponse> observable = apiInterface.codeThanks(request.getUserId(),request.getAppPlatform(),request.getAppVersion(),generateKey(request.getAppKey() + request.getUserId()));
        handleObservableResult(ApiAction.CODE_THANKS, observable);
    }

    public void log(LogRequest request){
        onApiRequestListener.onApiRequestStart(ApiAction.LOG);
        final Observable<BaseResponse> observable = apiInterface.log(request.getUserId(),
                request.getEmail(), request.getCode(),request.getScore(), generateKey("realscore"+request.getScore()),
                request.getAppPlatform(), request.getAppVersion(),
                generateKey(request.getAppKey() + request.getUserId()));
        handleObservableResult(ApiAction.LOG, observable);
    }

    public void getHighScore(){
        onApiRequestListener.onApiRequestStart(ApiAction.HIGHSCORE);
        final Observable<Response<ResponseBody>> observable = apiInterface.getHighScore();
        handleObservableResult(ApiAction.HIGHSCORE, observable);
    }


    private String generateKey(String string){
        String key = "";
        try {
            key = Sha1Generator.SHA1(string);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return key.isEmpty()?string:key;
    }
}
