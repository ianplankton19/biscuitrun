package com.summitconnect.biscuitrun.data.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jayan on 5/13/2017.
 */

public class BaseResponse {

    @SerializedName("success")
    int success;
    @SerializedName("message")
    String message;

    public int isSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
