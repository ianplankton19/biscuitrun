package com.summitconnect.biscuitrun.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.summitconnect.biscuitrun.models.User;

import java.util.List;

import lombok.Data;

/**
 * Created by jayan on 5/13/2017.
 */

@Data
public class LoginResponse extends BaseResponse {
    @SerializedName("posts")
    List<User> posts;
}
