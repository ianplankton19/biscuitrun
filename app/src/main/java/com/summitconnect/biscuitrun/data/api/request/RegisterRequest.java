package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.summitconnect.biscuitrun.enums.MODE;

/**
 * Created by jayan on 5/13/2017.
 */

public class RegisterRequest extends BaseRequest {

    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("name")
    private String name;
    @SerializedName("password")
    private String password;
    @SerializedName("age")
    private String age;
    @SerializedName("postal")
    private String postal;
    @SerializedName("mode")
    private String mode = MODE.register.name();

    public RegisterRequest(String email, String mobile, String name, String password, String age, String postal) {
        this.email = email;
        this.mobile = mobile;
        this.name = name;
        this.password = password;
        this.age = age;
        this.postal = postal;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
