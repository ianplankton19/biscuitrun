package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jayan on 5/13/2017.
 */

public class GameCodeRequest extends BaseRequest{

    @SerializedName("storeCode")
    String storeCode;
    @SerializedName("month")
    String month;
    @SerializedName("transId")
    String transId;
    @SerializedName("year")
    String year;
    @SerializedName("pos")
    String pos;
    @SerializedName("day")
    String day;
    @SerializedName("userId")
    String userId;

    public GameCodeRequest(String storeCode, String month, String transId, String year, String pos, String day, String userId) {
        this.storeCode = storeCode;
        this.month = month;
        this.transId = transId;
        this.year = year;
        this.pos = pos;
        this.day = day;
        this.userId = userId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
