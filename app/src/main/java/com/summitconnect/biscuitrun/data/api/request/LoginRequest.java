package com.summitconnect.biscuitrun.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.summitconnect.biscuitrun.enums.MODE;

/**
 * Created by jayan on 5/13/2017.
 */

public class LoginRequest extends  BaseRequest{
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("mode")
    private String mode = MODE.login.name();

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
