package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import com.summitconnect.biscuitrun.BuildConfig;
import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.WebviewHelper;
import com.summitconnect.biscuitrun.helpers.encryption.EncryptionUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class GameRulesFragment extends Fragment {

    @BindView(R.id.wvGameRules)
    WebView wvGameRules;
    @BindView(R.id.btn_close)
    ImageView btnClose;
    @BindView(R.id.btn_next)
    Button btnNext;

    @SuppressLint("ValidFragment")
    protected GameRulesFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static GameRulesFragment newInstance() {
        GameRulesFragment fragment = new GameRulesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_rules, container, false);
        ButterKnife.bind(this, view);
        WebviewHelper.loadUrl((MainActivity) getActivity(), wvGameRules, EncryptionUtil.decrypt(BuildConfig.AES,BuildConfig.BASE_URL) + "objects/rules.php");
        return view;
    }

    @OnClick({R.id.btn_close, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                EventBus.getDefault().post(new ChangeFragmentEvent(HomeFragment.newInstance()));
                break;
            case R.id.btn_next:
                EventBus.getDefault().post(new ChangeFragmentEvent(HowToPlayFragment.newInstance()));
                break;
        }
    }
}
