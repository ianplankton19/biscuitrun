package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.adapter.HighScoreAdapter;
import com.summitconnect.biscuitrun.base.BaseActivity;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.JSONUtility;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by jess on 20/05/2017.
 */

public class HighScoreFragment extends Fragment implements OnApiRequestListener {

    @BindView(R.id.rvHighScores)
    RecyclerView rvHighScores;

    @BindView(R.id.btn_ok)
    Button btn_ok;

    BaseActivity activity;

    ApiRequestHelper requestHelper;

    ArrayList<Map.Entry<String, JsonElement>> listHighScore;
    HighScoreAdapter highScoreAdapter;

    @SuppressLint("ValidFragment")
    protected HighScoreFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static HighScoreFragment newInstance() {
        HighScoreFragment fragment = new HighScoreFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_highscore, container, false);
        ButterKnife.bind(this, view);

        //get activity
        activity = (BaseActivity) getActivity();

        setRecyclerViewAdapter();

        getHighScores();

        return view;
    }

    private void getHighScores() {
        requestHelper = new ApiRequestHelper(this);

        if (activity.isConnected())
            requestHelper.getHighScore();
        else
            activity.showNoConnectionError();

    }

    private void setRecyclerViewAdapter() {

        listHighScore = new ArrayList<>();

        rvHighScores.setLayoutManager(new LinearLayoutManager(getActivity()));
        highScoreAdapter = new HighScoreAdapter(activity, listHighScore);
        rvHighScores.setAdapter(highScoreAdapter);
    }

    @OnClick(R.id.btn_ok)
    public void cancel(){
        EventBus.getDefault().post(new ChangeFragmentEvent(HomeFragment.newInstance()));


    }

    @Override
    public void onApiRequestStart(ApiAction apiAction) {

    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {

    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {

        Response<ResponseBody> highScoreResponse = (Response<ResponseBody>) result;


        try {
            String stringJson = highScoreResponse.body().string();
//            String stringJson = JSONUtility.demoJSON();
            Log.i("highscore", "highscore: " + stringJson);

            JsonObject jsonObject = JSONUtility.getAsJSONObject(stringJson);
            listHighScore.addAll(new ArrayList<>(jsonObject.entrySet()));

        } catch (IOException e) {
            e.printStackTrace();
        }


        highScoreAdapter.notifyDataSetChanged();
        rvHighScores.invalidate();


    }
}
