package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class GameCodeSkipFragment extends Fragment {


    @BindView(R.id.tv_message)
    TextView tvMessage;

    String message;

    @SuppressLint("ValidFragment")
    protected GameCodeSkipFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static GameCodeSkipFragment newInstance(String message) {
        GameCodeSkipFragment fragment = new GameCodeSkipFragment();
        fragment.message = message;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_skip_message, container, false);
        ButterKnife.bind(this, view);
        tvMessage.setText(message);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.btn_close)
    public void onViewClicked() {
        EventBus.getDefault().post(new ChangeFragmentEvent(HomeFragment.newInstance()));
    }
}
