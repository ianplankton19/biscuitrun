package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.summitconnect.biscuitrun.GameActivity;
import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.events.ExtraLifeEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class GameOverFragment extends Fragment {

    @BindViews({R.id.img_star_left, R.id.img_star_center, R.id.img_star_right})
    List<ImageView> imgStars;
    @BindView(R.id.txt_score)
    TextView txtScore;
    @BindView(R.id.btn_next)
    Button btnNext;

    MainActivity activity;

    boolean hasLife;
    int level;
    int selectedCharacter;
    int score;
    int generatedStarCount;
    int starCount;

    @SuppressLint("ValidFragment")
    protected GameOverFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static GameOverFragment newInstance(boolean hasLife, int level, int selectedCharacter, int score, int generatedStarCount) {
        GameOverFragment fragment = new GameOverFragment();
        fragment.hasLife = hasLife;
        fragment.level = level;
        fragment.selectedCharacter = selectedCharacter;
        fragment.score = score;
        fragment.generatedStarCount = generatedStarCount;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gameover, container, false);
        ButterKnife.bind(this, view);
        activity = (MainActivity) getActivity();
        double percentage;
        if (score > 0) {
            double i = (double) score / (double) generatedStarCount;
            percentage = i * 100;
            if (percentage >= 11 && percentage <= 40) {
                starCount = 1;
            } else if (percentage >= 41 && percentage <= 70) {
                starCount = 2;
            } else if (percentage >= 70) {
                starCount = 3;
            }
            for (int x = 0; x < starCount; x++) {
                imgStars.get(x).setImageResource(R.drawable.pickup_alert_sheet1);
            }
        }
        LogHelper.log("GameOverFragment", "StarCount: " + starCount);
        txtScore.setText("Score: " + score);

        if (hasLife) {
            btnNext.setVisibility(View.GONE);
        }

        //set music bg
        if(activity.isAudioOn) {
            activity.audioControl(AudioControlMode.PLAY, MainMusicType.GAMEOVER);
        }

        return view;
    }

    @OnClick({R.id.btn_next, R.id.btn_play_again, R.id.btn_know_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_know_more:
                EventBus.getDefault().post(new ChangeFragmentEvent(GameRulesFragment.newInstance()));
                break;
            case R.id.btn_next:
                EventBus.getDefault().post(new ChangeFragmentEvent(HighScoreFragment.newInstance()));
                break;
            case R.id.btn_play_again:
                if (hasLife) {
                    EventBus.getDefault().postSticky(new ExtraLifeEvent(true));
                    activity.setCode("free");
                    activity.goToOtherActivity(GameActivity.class);
                } else {
                    EventBus.getDefault().post(new ChangeFragmentEvent(StartGameCodeFragment.newInstance()));
                }
                break;
        }
    }
}
