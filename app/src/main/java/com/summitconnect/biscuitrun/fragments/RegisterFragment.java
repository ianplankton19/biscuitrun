package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.base.BaseActivity;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.data.api.request.RegisterRequest;
import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GenericResponse;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;
import com.summitconnect.biscuitrun.singletons.AnalyticsSingleton;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

public class RegisterFragment extends Fragment implements OnApiRequestListener {


    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @BindView(R.id.btn_close)
    ImageView btnClose;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.txt_name)
    EditText txtName;
    @BindView(R.id.txt_contact_number)
    EditText txtContactNumber;
    @BindView(R.id.txt_email)
    EditText txtEmail;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.txt_retype_password)
    EditText txtRetypePassword;
    @BindView(R.id.txt_age)
    EditText txtAge;
    @BindView(R.id.txt_postal)
    EditText txtPostal;

    private ApiRequestHelper requestHelper;
    private BaseActivity activity;

    boolean isAudioOn = true;

    // ===========================================================
    // Constructors
    // ===========================================================

    @SuppressLint("ValidFragment")
    protected RegisterFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);

        //get activity
        activity = (BaseActivity) getActivity();

        //set api helper
        requestHelper = new ApiRequestHelper(this);

        return view;
    }

    @Override
    public void onApiRequestStart(ApiAction apiAction) {
        activity.showLoading();
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {
        activity.dismissLoading();
        LogHelper.log("err", "action --> " + apiAction.name() + " cause --> " + t.getMessage());
        if (t.getMessage().contains("failed to connect") || t.getMessage().contains("Failed to connect")) {
            activity.showFailedToConnectError();
        } else if (t.getMessage().contains("timeout")) {
            activity.showSocketTimeoutError();
        } else if (t.getMessage().contains("No address associated with hostname") ||
                t.getMessage().contains("Unable to resolve host")) {
            activity.showNoConnectionError();
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = ((HttpException) t);
                try {
                    final String json = ex.response().errorBody().string();
                    final GenericResponse genericResponse = requestHelper.parseError(GenericResponse.class, json);
                    activity.showToast(genericResponse.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    activity.showToast(getString(R.string.all_message_error_unknown));
                }
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {
        activity.dismissLoading();
        if (apiAction.equals(ApiAction.REGISTER)) {
            if (((BaseResponse) result).isSuccess() == 1) {
                EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                activity.showToast("User Added");
                AnalyticsSingleton.getInstance().trackRegister();
            } else
                activity.showToast(((BaseResponse) result).getMessage());
        }
    }

    @OnClick({R.id.btn_close, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                break;
            case R.id.btn_submit:
                submit();
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void submit() {
        String name = txtName.getText().toString();
        String contactNo = txtContactNumber.getText().toString();
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        String age = txtAge.getText().toString();
        String postal = txtPostal.getText().toString();
        String retypePassword = txtRetypePassword.getText().toString();

        if (retypePassword.isEmpty()) {
            txtRetypePassword.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtRetypePassword.requestFocus();
        }

        if (password.isEmpty()) {
            txtPassword.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtPassword.requestFocus();
        }

        if (email.isEmpty()) {
            txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtEmail.requestFocus();
        } else {
            if (!activity.isValidEmailAddress(email)) {
                txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_invalidemailformat));
                txtEmail.requestFocus();
            }
        }

        if (postal.isEmpty()) {
            txtPostal.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtPostal.requestFocus();
        }

        if (contactNo.isEmpty()) {
            txtContactNumber.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtContactNumber.requestFocus();
        }

        if (age.isEmpty()) {
            txtAge.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtAge.requestFocus();
        }

        if (name.isEmpty()) {
            txtName.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtName.requestFocus();
        }

        if (txtName.getError() == null && txtContactNumber.getError() == null &&
                txtAge.getError() == null && txtPostal.getError() == null &&
                txtEmail.getError() == null && txtPassword.getError() == null &&
                txtRetypePassword.getError() == null) {

            if (!password.equals(retypePassword)) {
                txtRetypePassword.setError("Password do not match.");
                txtRetypePassword.requestFocus();
            } else {
                if (activity.isConnected())
                    requestHelper.register(new RegisterRequest(email, contactNo, name, password, age, postal));
                else
                    activity.showNoConnectionError();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
