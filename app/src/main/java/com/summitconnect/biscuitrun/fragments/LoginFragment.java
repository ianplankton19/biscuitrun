package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.summitconnect.biscuitrun.BuildConfig;
import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.data.api.request.LoginRequest;
import com.summitconnect.biscuitrun.data.api.response.GenericResponse;
import com.summitconnect.biscuitrun.data.api.response.LoginResponse;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

public class LoginFragment extends Fragment implements OnApiRequestListener {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    @BindView(R.id.txt_email)
    EditText txtEmail;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.btn_forgot_password)
    Button btnForgotPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.btn_audio_toggle)
    Button btnAudioToggle;

    private ApiRequestHelper requestHelper;
    private MainActivity activity;


    // ===========================================================
    // Constructors
    // ===========================================================

    @SuppressLint("ValidFragment")
    protected LoginFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        //get activity
        activity = (MainActivity) getActivity();

        //set api helper
        requestHelper = new ApiRequestHelper(this);

        //set mock user
        if (BuildConfig.DEBUG) {
            txtEmail.setText("test.mobile@gmail.com");
            txtPassword.setText("password123");
        }

        setAudioButton();


        return view;
    }

    @OnClick(R.id.btn_audio_toggle)
    public void audioToggle(){
        if (activity.isAudioOn){
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_off);
            activity.audioControl(AudioControlMode.STOP, MainMusicType.LOBBY);
        }else{
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_on);
            activity.audioControl(AudioControlMode.PLAY, MainMusicType.LOBBY);
        }
    }

    @OnClick(R.id.btn_login)
    public void login(){
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();

        if (password.isEmpty()) {
            txtPassword.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtPassword.requestFocus();
        }

        if (email.isEmpty()) {
            txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
            txtEmail.requestFocus();
        }else {
            if (!activity.isValidEmailAddress(email)) {
                txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_invalidemailformat));
                txtEmail.requestFocus();
            }
        }

        if (txtEmail.getError() == null && txtPassword.getError() == null){
            if (activity.isConnected())
                requestHelper.login(new LoginRequest(email,password));
            else
                activity.showNoConnectionError();

        }
    }

    @OnClick(R.id.btn_signup)
    public void signUp(){
        EventBus.getDefault().post(new ChangeFragmentEvent(RegisterFragment.newInstance()));
    }

    @OnClick(R.id.btn_forgot_password)
    public void forgotPassword(){
        EventBus.getDefault().post(new ChangeFragmentEvent(ResetPasswordFragment.newInstance()));
    }

    @Override
    public void onApiRequestStart(ApiAction apiAction) {
        activity.showLoading();
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {
        activity.dismissLoading();
        LogHelper.log("err", "action --> " + apiAction.name() + " cause --> " + t.getMessage());
        if (t.getMessage().contains("failed to connect") || t.getMessage().contains("Failed to connect")) {
            activity.showFailedToConnectError();
        } else if (t.getMessage().contains("timeout")) {
            activity.showSocketTimeoutError();
        } else if (t.getMessage().contains("No address associated with hostname") ||
                t.getMessage().contains("Unable to resolve host")) {
            activity.showNoConnectionError();
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = ((HttpException) t);
                try {
                    final String json = ex.response().errorBody().string();
                    final GenericResponse genericResponse = requestHelper.parseError(GenericResponse.class, json);
                    activity.showToast(genericResponse.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    activity.showToast(getString(R.string.all_message_error_unknown));
                }
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {
        activity.dismissLoading();
        if (apiAction.equals(ApiAction.LOGIN)) {
            if (((LoginResponse)result).isSuccess() == 1) {
                EventBus.getDefault().post(new ChangeFragmentEvent(HomeFragment.newInstance()));
                activity.setUserId((((LoginResponse)result).getPosts().get(0).getUserId()));
                activity.setEmail((((LoginResponse)result).getPosts().get(0).getEmail()));
            }else
                activity.showToast(((LoginResponse)result).getMessage());
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void setAudioButton(){
        if (activity.isAudioOn){
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_on);
        }else{
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_off);
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
