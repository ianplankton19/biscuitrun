package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.base.BaseActivity;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.data.api.request.ForgotPasswordRequest;
import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GenericResponse;
import com.summitconnect.biscuitrun.data.api.response.LoginResponse;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by jess on 17/05/2017.
 */

public class ResetPasswordFragment extends Fragment implements OnApiRequestListener {
    @BindView(R.id.txt_email)
    EditText txtEmail;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.txt_retype_password)
    EditText txtRetypePassword;

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private ApiRequestHelper requestHelper;
    private BaseActivity activity;

    // ===========================================================
    // Constructors
    // ===========================================================
    @SuppressLint("ValidFragment")
    protected ResetPasswordFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static ResetPasswordFragment newInstance() {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        return fragment;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        ButterKnife.bind(this, view);

        //get activity
        activity = (BaseActivity) getActivity();

        //set api helper
        requestHelper = new ApiRequestHelper(this);

        return view;
    }

    @OnClick({R.id.btn_close, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                break;
            case R.id.btn_submit:
                String email = txtEmail.getText().toString();
                String password = txtPassword.getText().toString();
                String retypePassword = txtRetypePassword.getText().toString();

                if (retypePassword.isEmpty()) {
                    txtRetypePassword.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
                    txtRetypePassword.requestFocus();
                }

                if (password.isEmpty()) {
                    txtPassword.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
                    txtPassword.requestFocus();
                }

                if (email.isEmpty()) {
                    txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_requiredfield));
                    txtEmail.requestFocus();
                }else {
                    if (!activity.isValidEmailAddress(email)) {
                        txtEmail.setError(activity.getStringFromResource(R.string.all_message_error_invalidemailformat));
                        txtEmail.requestFocus();
                    }
                }

                if (txtEmail.getError() == null && txtPassword.getError() == null &&
                        txtRetypePassword.getError() == null){

                    if (!password.equals(retypePassword)){
                        txtRetypePassword.setError("Password do not match.");
                        txtRetypePassword.requestFocus();
                    }else{
                        if (activity.isConnected())
                            requestHelper.forgotPassword(new ForgotPasswordRequest(email,password));
                        else
                            activity.showNoConnectionError();
                    }
                }
                break;
        }
    }

    @Override
    public void onApiRequestStart(ApiAction apiAction) {
        activity.showLoading();
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {
        activity.dismissLoading();
        LogHelper.log("err", "action --> " + apiAction.name() + " cause --> " + t.getMessage());
        if (t.getMessage().contains("failed to connect") || t.getMessage().contains("Failed to connect")) {
            activity.showFailedToConnectError();
        } else if (t.getMessage().contains("timeout")) {
            activity.showSocketTimeoutError();
        } else if (t.getMessage().contains("No address associated with hostname") ||
                t.getMessage().contains("Unable to resolve host")) {
            activity.showNoConnectionError();
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = ((HttpException) t);
                try {
                    final String json = ex.response().errorBody().string();
                    final GenericResponse genericResponse = requestHelper.parseError(GenericResponse.class, json);
                    activity.showToast(genericResponse.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    activity.showToast(getString(R.string.all_message_error_unknown));
                }
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {
        activity.dismissLoading();
        if (apiAction.equals(ApiAction.FORGOT_PASSWORD)) {
            if (((BaseResponse) result).isSuccess() == 1) {
                EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                activity.showToast("Check your email to complete password reset.");
            }else
                activity.showToast(((LoginResponse) result).getMessage());
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
