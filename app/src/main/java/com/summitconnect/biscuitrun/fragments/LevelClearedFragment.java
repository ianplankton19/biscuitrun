package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.StartOnGameActivityEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class LevelClearedFragment extends Fragment {

    @BindView(R.id.btn_next)
    Button btnNext;
    @BindViews({R.id.img_star_left,R.id.img_star_center,R.id.img_star_right})
    List<ImageView> imgStars;

    boolean hasLife;
    int nextLevel;
    int score;
    int generatedStar;
    int selectedCharacter;
    private int starCount;

    MainActivity activity;

    @SuppressLint("ValidFragment")
    protected LevelClearedFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static LevelClearedFragment newInstance(boolean hasLife,int nextLevel, int score, int generatedStar, int selectedCharacter) {
        LevelClearedFragment fragment = new LevelClearedFragment();
        fragment.hasLife = hasLife;
        fragment.nextLevel = nextLevel;
        fragment.score = score;
        fragment.generatedStar = generatedStar;
        fragment.selectedCharacter = selectedCharacter;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_level_cleared, container, false);
        ButterKnife.bind(this, view);
        activity = (MainActivity) getActivity();

        double percentage;
        if (score > 0) {
            double i = (double) score / (double) generatedStar;
            percentage = i * 100;
            if (percentage >= 11 && percentage <= 40) {
                starCount = 1;
            } else if (percentage >= 41 && percentage <= 70) {
                starCount = 2;
            } else if (percentage >= 70) {
                starCount = 3;
            }
            for (int x = 0; x < starCount; x++) {
                imgStars.get(x).setImageResource(R.drawable.pickup_alert_sheet1);
            }
        }

        //set music bg
        if(activity.isAudioOn) {
            activity.audioControl(AudioControlMode.PLAY, MainMusicType.LVL_CLEAR);
        }

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        EventBus.getDefault().postSticky(new StartOnGameActivityEvent(nextLevel,selectedCharacter,score,generatedStar,hasLife));
    }
}
