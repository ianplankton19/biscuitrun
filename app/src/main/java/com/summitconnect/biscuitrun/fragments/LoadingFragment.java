package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jess on 17/05/2017.
 */

public class LoadingFragment extends Fragment {
    @BindView(R.id.tv_message)
    TextView tvMessage;

    String message;

    MainActivity activity;

    @SuppressLint("ValidFragment")
    protected LoadingFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static LoadingFragment newInstance(String message) {
        LoadingFragment fragment = new LoadingFragment();
        fragment.message = message;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading, container, false);
        ButterKnife.bind(this, view);
        activity = (MainActivity) getActivity();

        if (!message.isEmpty())tvMessage.setText(message);


        activity.doNotPlaySound(); // don't play audio on loading


        return view;
    }
}
