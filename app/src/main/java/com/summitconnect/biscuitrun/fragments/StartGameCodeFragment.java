package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.summitconnect.biscuitrun.BuildConfig;
import com.summitconnect.biscuitrun.GameActivity;
import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.data.api.ApiAction;
import com.summitconnect.biscuitrun.data.api.ApiRequestHelper;
import com.summitconnect.biscuitrun.data.api.OnApiRequestListener;
import com.summitconnect.biscuitrun.data.api.request.GameCodeRequest;
import com.summitconnect.biscuitrun.data.api.request.LastLogRequest;
import com.summitconnect.biscuitrun.data.api.response.BaseResponse;
import com.summitconnect.biscuitrun.data.api.response.GameCodeResponse;
import com.summitconnect.biscuitrun.data.api.response.GenericResponse;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.helpers.LogHelper;
import com.summitconnect.biscuitrun.singletons.AnalyticsSingleton;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by jess on 17/05/2017.
 */

public class StartGameCodeFragment extends Fragment  implements OnApiRequestListener{

    @BindView(R.id.txt_store_code)
    EditText txtStoreCode;
    @BindView(R.id.txt_month)
    EditText txtMonth;
    @BindView(R.id.txt_trans_id)
    EditText txtTransId;
    @BindView(R.id.txt_year)
    EditText txtYear;
    @BindView(R.id.txt_pos)
    EditText txtPos;
    @BindView(R.id.txt_day)
    EditText txtDay;
    @BindView(R.id.btn_close)
    ImageView btnClose;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btn_skip)
    Button btnSkip;
    @BindView(R.id.btn_game_code_details)
    Button btnGameCodeDetails;

    private ApiRequestHelper requestHelper;
    private MainActivity activity;

    @SuppressLint("ValidFragment")
    protected StartGameCodeFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static StartGameCodeFragment newInstance() {
        StartGameCodeFragment fragment = new StartGameCodeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_game_code, container, false);
        ButterKnife.bind(this, view);
        activity = (MainActivity) getActivity();

        //get activity
        activity = (MainActivity) getActivity();
        activity.setCode("none");
        //set api helper
        requestHelper = new ApiRequestHelper(this);

        //default text for test
        if (BuildConfig.DEBUG) {
            txtStoreCode.setText("1");
            txtDay.setText("1");
            txtMonth.setText("1");
            txtPos.setText("1");
            txtTransId.setText("1");
            txtYear.setText("1");
        }

        return view;
    }

    @OnClick({R.id.btn_close, R.id.btn_skip, R.id.btn_submit, R.id.btn_game_code_details})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                EventBus.getDefault().post(new ChangeFragmentEvent(HomeFragment.newInstance()));
                break;
            case R.id.btn_skip:
                if (activity.isConnected()){
                    requestHelper.lastLog(new LastLogRequest(activity.getUserId()));
                }else {
                    activity.showNoConnectionError();
                }
                break;
            case R.id.btn_submit:
                String storeCode = txtStoreCode.getText().toString();
                String month = txtMonth.getText().toString();
                String transId = txtTransId.getText().toString();
                String year = txtYear.getText().toString();
                String pos = txtPos.getText().toString();
                String day = txtDay.getText().toString();

                if (storeCode.isEmpty() || month.isEmpty() || transId.isEmpty() ||
                        year.isEmpty() || pos.isEmpty() || day.isEmpty()){
                    activity.showToast("Incomplete Input");
                }else {
                    if (activity.isConnected()){
                        requestHelper.addGameCode(new GameCodeRequest(storeCode,month,transId,year,pos,day,activity.getUserId()));
                    }else {
                        activity.showNoConnectionError();
                    }
                }
                break;
            case R.id.btn_game_code_details:
                EventBus.getDefault().post(new ChangeFragmentEvent(GameCodeDetailsFragment.newInstance()));
                break;

        }
    }

    @Override
    public void onApiRequestStart(ApiAction apiAction) {
        activity.showLoading();
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {
        activity.dismissLoading();
        LogHelper.log("err", "action --> " + apiAction.name() + " cause --> " + t.getMessage());
        if (t.getMessage().contains("failed to connect") || t.getMessage().contains("Failed to connect")) {
            activity.showFailedToConnectError();
        } else if (t.getMessage().contains("timeout")) {
            activity.showSocketTimeoutError();
        } else if (t.getMessage().contains("No address associated with hostname") ||
                t.getMessage().contains("Unable to resolve host")) {
            activity.showNoConnectionError();
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = ((HttpException) t);
                try {
                    final String json = ex.response().errorBody().string();
                    final GenericResponse genericResponse = requestHelper.parseError(GenericResponse.class, json);
                    activity.showToast(genericResponse.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    activity.showToast(getString(R.string.all_message_error_unknown));
                }
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {
        activity.dismissLoading();
        if (apiAction.equals(ApiAction.LAST_LOG)) {
            if (((BaseResponse)result).isSuccess() == 1) {
                activity.goToOtherActivity(GameActivity.class);
            }else{
                String message = ((BaseResponse)result).getMessage();
                EventBus.getDefault().post(new ChangeFragmentEvent(GameCodeSkipFragment.newInstance(message)));
            }
        }else if(apiAction.equals(ApiAction.ADD_GAMECODE)){
            if (((GameCodeResponse)result).isSuccess() == 1) {
                StringBuilder builder = new StringBuilder();
                builder.append(txtStoreCode.getText().toString())
                        .append("-")
                        .append(txtMonth.getText().toString())
                        .append("-")
                        .append(txtTransId.getText().toString())
                        .append("-")
                        .append(txtYear.getText().toString())
                        .append("-")
                        .append(txtPos.getText().toString())
                        .append("-")
                        .append(txtDay.getText().toString());
                activity.setCode(builder.toString());
                requestHelper.codeThanks(new LastLogRequest(activity.getUserId()));
                AnalyticsSingleton.getInstance().trackSubmitGameCode();
            }else
                activity.showToast(((GameCodeResponse)result).getMessage());
        }else if(apiAction.equals(ApiAction.CODE_THANKS)){
            if (((BaseResponse)result).isSuccess() == 1) {
                EventBus.getDefault().post(new ChangeFragmentEvent(GameCodeAddedFragment.newInstance(((BaseResponse)result).getMessage())));
            }else{
                activity.showToast(((BaseResponse)result).getMessage());
            }
        }
    }


}
