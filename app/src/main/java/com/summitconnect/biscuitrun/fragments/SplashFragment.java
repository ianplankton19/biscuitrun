package com.summitconnect.biscuitrun.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jess on 17/05/2017.
 */

public class SplashFragment extends Fragment {


    MainActivity activity;
    @BindView(R.id.img_logo)
    ImageView imgLogo;

    @SuppressLint("ValidFragment")
    protected SplashFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        ButterKnife.bind(this, view);
        activity = (MainActivity) getActivity();
        fadeInAnim();
        return view;
    }

    private void fadeInAnim() {
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(imgLogo, "alpha", .3f, 1f);
        fadeIn.setDuration(1000);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(fadeIn);
        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //Delay opening activity
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                    }
                }, 2000);
            }
        });
        mAnimationSet.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activity.audioControl(AudioControlMode.PLAY, MainMusicType.LOBBY);
    }
}
