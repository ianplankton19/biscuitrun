package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.summitconnect.biscuitrun.GameActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class GameCodeAddedFragment extends Fragment {


    @BindView(R.id.tv_message)
    TextView tvMessage;

    BaseActivity activity;
    String message;

    @SuppressLint("ValidFragment")
    protected GameCodeAddedFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static GameCodeAddedFragment newInstance(String message) {
        GameCodeAddedFragment fragment = new GameCodeAddedFragment();
        fragment.message = message;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gamecode_message, container, false);
        ButterKnife.bind(this, view);
        activity = (BaseActivity) getActivity();
        tvMessage.setText(message);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        activity.goToOtherActivity(GameActivity.class);
    }
}
