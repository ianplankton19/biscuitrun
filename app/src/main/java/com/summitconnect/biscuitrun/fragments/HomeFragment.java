package com.summitconnect.biscuitrun.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.summitconnect.biscuitrun.MainActivity;
import com.summitconnect.biscuitrun.R;
import com.summitconnect.biscuitrun.enums.AudioControlMode;
import com.summitconnect.biscuitrun.enums.MainMusicType;
import com.summitconnect.biscuitrun.events.ChangeFragmentEvent;
import com.summitconnect.biscuitrun.singletons.AnalyticsSingleton;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jess on 17/05/2017.
 */

public class HomeFragment extends Fragment {

    MainActivity activity;

    @BindView(R.id.btn_audio_toggle)
    Button btnAudioToggle;

    @SuppressLint("ValidFragment")
    protected HomeFragment() {
        // Use to prevent instantiating fragment using new constructor.
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        //get activity
        activity = (MainActivity) getActivity();

        setAudioButton();

        if(((MainActivity)activity).isAudioOn && !((MainActivity)activity).isAudioPlaying()) {
            //Play if audio is ON and audio media player is not playing
            ((MainActivity)activity).audioControl(AudioControlMode.PLAY, MainMusicType.LOBBY);
        }

        return view;
    }

    @OnClick({R.id.btn_logout, R.id.btn_audio_toggle, R.id.btn_about, R.id.btn_game_rules, R.id.btn_start, R.id.btn_how_to_play, R.id.btn_high_score, R.id.btn_sponsors})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_logout:
                EventBus.getDefault().post(new ChangeFragmentEvent(LoginFragment.newInstance()));
                break;
            case R.id.btn_audio_toggle:
                if (activity.isAudioOn){
                    btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_off);
                    activity.audioControl(AudioControlMode.STOP, MainMusicType.LOBBY);
                }else{
                    btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_on);
                    activity.audioControl(AudioControlMode.PLAY, MainMusicType.LOBBY);
                }
                break;
            case R.id.btn_about:
                EventBus.getDefault().post(new ChangeFragmentEvent(AboutFragment.newInstance()));
                break;
            case R.id.btn_game_rules:
                EventBus.getDefault().post(new ChangeFragmentEvent(GameRulesFragment.newInstance()));
                break;
            case R.id.btn_start:
                EventBus.getDefault().post(new ChangeFragmentEvent(StartGameCodeFragment.newInstance()));
                AnalyticsSingleton.getInstance().trackStartGame();
                break;
            case R.id.btn_how_to_play:
                EventBus.getDefault().post(new ChangeFragmentEvent(HowToPlayFragment.newInstance()));
                break;
            case R.id.btn_high_score:
                EventBus.getDefault().post(new ChangeFragmentEvent(HighScoreFragment.newInstance()));
                break;
            case R.id.btn_sponsors:
                EventBus.getDefault().post(new ChangeFragmentEvent(SponsorsFragment.newInstance()));
                break;
        }
    }

    public void setAudioButton(){
        if (activity.isAudioOn){
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_on);
        }else{
            btnAudioToggle.setBackgroundResource(R.drawable.btn_audiotoggle_off);
        }
    }
}
