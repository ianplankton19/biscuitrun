package com.summitconnect.biscuitrun.scenes;

import com.summitconnect.biscuitrun.base.BaseScene;
import com.summitconnect.biscuitrun.characters.Biscuit;
import com.summitconnect.biscuitrun.characters.ChocoKnots;
import com.summitconnect.biscuitrun.characters.Cookies;
import com.summitconnect.biscuitrun.events.StartOnGameActivityEvent;
import com.summitconnect.biscuitrun.helpers.PositionHelper;
import com.summitconnect.biscuitrun.managers.SceneManager;
import com.summitconnect.biscuitrun.singletons.AnalyticsSingleton;

import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by jayan on 5/14/2017.
 */

public class ChooseCharacterScene extends BaseScene implements Biscuit.Callback,
        Cookies.Callback, ChocoKnots.Callback {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Text textCookie;
    private Text textBiscuit;
    private Text textPretzel;
    private ChocoKnots mChocoKnots;
    private Cookies mCookies;
    private Biscuit mBiscuit;
    private Sprite mSelect;
    private Sprite mTitle;
    private int mIsSelected
            ;
    private Sprite mGlass;

    boolean isTapCharacter;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void createScene() {
        setBackground(new SpriteBackground(new Sprite(0, 0, resourcesManager.backgroundTextureRegion,vbom)));
        mChocoKnots = new ChocoKnots(876, 351, resourcesManager.mFaceTextureRegionChocoKnot, vbom, 3, this);
        mCookies = new Cookies(52, 346, resourcesManager.mFaceTextureRegionCookie, vbom, 2, this);
        mBiscuit = new Biscuit(539, 325, resourcesManager.mFaceTextureRegionBiscuit, vbom, 0, this);
        textCookie = new Text(0, 0, this.resourcesManager.font, "Chloe Cookie", resourcesManager.activity.getVertexBufferObjectManager());
        textBiscuit = new Text(0, 0, this.resourcesManager.font, "Bobby Biscuit", resourcesManager.activity.getVertexBufferObjectManager());
        textPretzel = new Text(0, 0, this.resourcesManager.font, "Peter Pretzel", resourcesManager.activity.getVertexBufferObjectManager());

        //re-position characters
        mBiscuit.setPosition(PositionHelper.centerXscreen(resourcesManager.camera, mBiscuit) ,350);

        //re-position of text
        float nameXPos = ((mCookies.getY() - textCookie.getHeight()) - 25);
        textCookie.setPosition(PositionHelper.centerXparent(mCookies,textCookie), nameXPos);
        textBiscuit.setPosition(PositionHelper.centerXparent(mBiscuit,textBiscuit), nameXPos);
        textPretzel.setPosition(PositionHelper.centerXparent(mChocoKnots,textPretzel), nameXPos);



        mSelect = new Sprite(457, 532, resourcesManager.mTextureRegionSelect, vbom){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionUp()){
                    if (mIsSelected > 0) {
                        mChocoKnots.stopAnimation();
                        mCookies.stopAnimation();
                        resourcesManager.activity.playSound(resourcesManager.soundSelect);
                        EventBus.getDefault().postSticky(new StartOnGameActivityEvent(1, mIsSelected,0, 0, false));
                        getActivity().finish();
                        AnalyticsSingleton.getInstance().trackSelectCharacter(mIsSelected);
                    }
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };

        //re-position select button
        mSelect.setPosition(PositionHelper.centerXscreen(resourcesManager.camera, mSelect), PositionHelper.bottomOfScreen(resourcesManager.camera, mSelect));

        mTitle = new Sprite(84, 93, resourcesManager.mITextureRegionTitle, vbom);

        mGlass = new Sprite(0 - resourcesManager.mTextureGlass.getWidth(), 257, resourcesManager.mITextureRegionGlass, vbom);

        registerTouchArea(mChocoKnots);
        registerTouchArea(mBiscuit);
        registerTouchArea(mCookies);
        registerTouchArea(mSelect);
        setTouchAreaBindingOnActionDownEnabled(true);

        attachChild(mGlass);
        attachChild(mCookies);
        attachChild(mChocoKnots);
        attachChild(mBiscuit);
        attachChild(mTitle);
        attachChild(textCookie);
        attachChild(textBiscuit);
        attachChild(textPretzel);

        this.resourcesManager.activity.playMusic();
    }

    @Override
    public void onBackKeyPressed() {
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_CHOOSECHARACTER;
    }

    @Override
    public void disposeScene() {
        mChocoKnots.detachSelf();
        mChocoKnots.dispose();
        mCookies.detachSelf();
        mCookies.dispose();
        mBiscuit.detachSelf();
        mBiscuit.dispose();
        mSelect.detachSelf();
        mSelect.dispose();
        mTitle.detachSelf();
        mTitle.dispose();
        mGlass.detachSelf();
        mGlass.dispose();
        mIsSelected = 0;
        this.detachSelf();
        this.dispose();
    }

    @Override
    public void characterBiscuitChosen() {
        mIsSelected = 2;
        mGlass.setPosition(429, 178);
        mChocoKnots.onDeselect();
        mCookies.onDeselect();
        resourcesManager.activity.playSound(this.resourcesManager.soundCharChoose);

        if (!isTapCharacter)
            attachChild(mSelect);

        isTapCharacter = true;

    }

    @Override
    public void characterOnCookieChosen() {
        mIsSelected = 1;
        mGlass.setPosition(19, 178);
        mBiscuit.onDeselect();
        mChocoKnots.onDeselect();
        resourcesManager.activity.playSound(this.resourcesManager.soundCharChoose);

        if (!isTapCharacter)
            attachChild(mSelect);

        isTapCharacter = true;

    }

    @Override
    public void characterOnChocoKnotChosen() {
        mIsSelected = 3;
        mGlass.setPosition(845, 178);
        mBiscuit.onDeselect();
        mCookies.onDeselect();
        resourcesManager.activity.playSound(this.resourcesManager.soundCharChoose);

        if (!isTapCharacter)
            attachChild(mSelect);

        isTapCharacter = true;

    }
    // ===========================================================
    // Methods
    // ===========================================================



    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
