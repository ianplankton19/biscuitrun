# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\jayan\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keep class org.apache.http.** { *; }
-keepclassmembers public class org.apache.http.** { *; }
-dontwarn org.apache.http.**

-keepnames class com.summitconnect.biscuitrun.events.*
-keepnames class com.summitconnect.biscuitrun.enums.*

-keep class com.summitconnect.biscuitrun.events.** {*;}
-keep class com.summitconnect.biscuitrun.enums.** {*;}

-dontwarn com.summitconnect.biscuitrun.data.api.request.**
-dontwarn com.summitconnect.biscuitrun.data.api.response.**
-dontwarn com.summitconnect.biscuitrun.events.**
-dontwarn com.summitconnect.biscuitrun.enums.**
